# keepass-view
CLI tool to view KeePass2 database (kdbx files).

# Intention
The main reason for this tool is to be able to read password from kdbx file in command line.
Editing will probably never be supported.

# Secure notes
XML representation is currently stored in plain text in the memory.

Password stream is stored in special vector implementation that makes sure all sensitive data is cleared before releasing.
This helps to ensure that after program exits no passwords are stored in memory.

At the moment passwords will be stored in plain text on decoding. This is true only for requested password.
Full stream decoding is not hapenning. Memory containing password will be wiped on exit.

# Implementation Notes
Currently using following fileformat description:
https://gist.githubusercontent.com/msmuenchen/9318327/raw/60d16b3faf5c680dee2be9d8b5cbfe877706f004/gistfile1.txt

Differences from the description that was found:
- All AES128 should be AES256 (cbc and ecb modes)
- Salsa20 key is Sha256(PROTECTEDSTREAMKEY)
