#include <unistd.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <string.h>
#include <termios.h>

#include "keepass_view_config.h"
#include "src/common/verbose_stream.h"
#include "src/common/debug_stream.h"
#include "src/format/kdbx_file.h"

void print_help()
{
	std::cout << "keepass-view [-h] [-f <file>]" << std::endl
			<< std::endl
			<< " Version: " << keepass_view_VERSION_MAJOR << "." << keepass_view_VERSION_MINOR << std::endl
			<< std::endl
			<< "-h               Print this message" << std::endl
			<< "-f <file_name>   <file_name> to open." << std::endl
			<< "-p               Turns on password prompt. This password will be used to decrypt the file." << std::endl
			<< "-P <pass>        Use <pass> to decrypt file. Not recomended." << std::endl
			<< "-K <file_name>   Use <file_name> content to decrypt file." << std::endl
			<< "-l <level>       Sets log level for prints. <level> can be normal, verbose, debug. Default is normal." << std::endl
			<< "-u <UUID>        If set then prints full entry with password, that matches UUID. Otherwise prints all tree without passwords." << std::endl
			<< std::endl
			<< "          keepas-view -P <pass> -f <file>            # would print tree of the file with UUID of the entries." << std::endl
			<< std::endl
			<< "          keepas-view -P <pass> -f <file> -u <UUID>  # would print specifit entry by UUID with password decrypted." << std::endl
			<< std::endl
			<< "          keepas-view -p -f <file>                   # would print tree of the file with UUID of the entries." << std::endl
			<< std::endl
			<< "          keepas-view -p -f <file> -u <UUID>         # would print specifit entry by UUID with password decrypted." << std::endl
			;
}

int main(int argc, char* argv[])
{
	char* filename = nullptr;
	bool  is_password = false;
	bool  is_cmd_line_password = false;
	SecureVector<uint8_t> cmd_line_password;
	char* log_level = nullptr;
	char* uuid = nullptr;
	std::string key_file_name;

	int c;
	while ((c = getopt (argc, argv, "hpf:l:u:P:K:")) != -1)
	{
		switch (c)
		{
			case 'f':
				filename = optarg;
				break;
			case 'p':
				is_password = true;
				break;
			case 'P':
				{
					is_cmd_line_password = true;
					size_t arg_len = strlen(optarg);
					cmd_line_password.resize(arg_len);
					cmd_line_password.assign(optarg, optarg + arg_len);
				}
				break;
			case 'K':
				key_file_name = std::string(optarg);
				break;
			case 'l':
				log_level = optarg;
				break;
			case 'u':
				uuid = optarg;
				break;
			case 'h':
				print_help();
				return 0;
			default:
				std::cerr << "Unknown or not implemented option" << std::endl;
				return -1;
		}
	}

	// Some sanity checks
	if (is_password && is_cmd_line_password)
	{
		std::cerr << "-p and -P options can't be used at the same time." << std::endl;
		return -1;
	}

	// Turn on verbose level if reqired
	if (log_level && (strcmp(log_level, "verbose") == 0 || strcmp(log_level, "debug") == 0))
		VerboseStream::Instance()->TurnOn();

	// Turn on debug level if reqired
	if (log_level && strcmp(log_level, "debug") == 0)
		DebugStream::Instance()->TurnOn();

	// Print version
	*VerboseStream::Instance() << "Running keepass-view-" << keepass_view_VERSION_MAJOR << "." << keepass_view_VERSION_MINOR << "\n";

	// Check filename
	if (!filename)
	{
		std::cerr << "Filename is missing" << std::endl;
		return -1;
	}
	*VerboseStream::Instance() << "DB file to read: " << filename << "\n";

	// Open File to work with
	std::ifstream file(filename, std::ios::in | std::ios::binary);
	if (!file.is_open())
	{
		std::cerr << "Error openning '" << filename << "'" << std::endl;
		return -1;
	}

	// Contribute data to composite key for payload decryption.
	KDBXFile kdbx_file;
	// Password
	if (is_password)
	{
		// Get pass from input
		std::cout << "Password: " << std::flush;

		// Hide input
		struct termios tty_orig;
		struct termios tty_new;
		tcgetattr(STDIN_FILENO, &tty_orig);
		tty_new = tty_orig;
		tty_new.c_lflag &= ~ECHO;
		tcsetattr(STDIN_FILENO, TCSANOW, &tty_new);

		SecureVector<uint8_t> password;
		std::cin >> password;

		// Restore terminal attributes
		tcsetattr(STDIN_FILENO, TCSANOW, &tty_orig);

		// Just clean "Password" prompt
		std::cout << "\r                            " << std::flush;
		std::cout << "\r" << std::flush;

		if (!kdbx_file.CompositeKeyAdd(password.data(), password.size()))
		{
			std::cerr << "Failed to add password to composite key" << std::endl;
			return -1;
		}
	}

	if (is_cmd_line_password)
	{
		if (!kdbx_file.CompositeKeyAdd(cmd_line_password.data(), cmd_line_password.size()))
		{
			std::cerr << "Failed to add password to composite key" << std::endl;
			return -1;
		}
	}

	if (key_file_name.size() != 0)
	{
		std::ifstream is;
		is.open(key_file_name.c_str(), std::ios::binary);
		if (is)
		{
			is.seekg(0, std::ios::end);
			int length = is.tellg();
			is.seekg(0, std::ios::beg);
			SecureVector<uint8_t> key_file_content;
			key_file_content.resize(length);
			is.read(reinterpret_cast<char*>(key_file_content.data()), length);
			if (!is)
				std::cerr << "Failed to read key from file" << std::endl;
			if (!kdbx_file.CompositeKeyAdd(key_file_content))
				std::cerr << "Failed to add key to composite key" << std::endl;
			is.close();
		}
		else
		{
			std::cerr << "Failed to open key file. Will try decrypting eitherway." << std::endl;
		}
	}

	// Read kdbx file
	if (!kdbx_file.Read(file))
		return -1;

	// Print tree by default or
	// print specific uuid if asked
	if (uuid)
	{
		if (!kdbx_file.PrintUUID(std::string(uuid)))
			return -1;
	} else {
		if (!kdbx_file.PrintTree())
			return -1;
	}

	file.close();

	return 0;
}
