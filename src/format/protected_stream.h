#ifndef PROTECTED_STREAM_H
#define PROTECTED_STREAM_H

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header.h"
#include "src/crypto/crypto.h"

class ProtectedStream
{
	KDBXFileHeader *header;
	SecureVector<uint8_t> IV = {0xE8, 0x30, 0x09, 0x4B, 0x97, 0x20, 0x5D, 0x2A};
	Salsa20 cipher;
	Sha256 sha256;
public:
	void SetHeader(KDBXFileHeader* header);

	void SkipBlock(const size_t bytes);
	SecureVector<uint8_t> DecryptBlock(const SecureVector<uint8_t>& data);

protected:
	void InitCypher();
};

#endif // PROTECTED_STREAM_H
