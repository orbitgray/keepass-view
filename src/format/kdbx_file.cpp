#include "src/common/print_hex.h"
#include "src/common/verbose_stream.h"
#include "src/common/secure_vector.h"
#include "src/format/kdbx_file.h"

#include <assert.h>
#include <iostream>

KDBXFile::KDBXFile()
{
}

bool KDBXFile::Read(std::ifstream &file)
{
	*VerboseStream::Instance() << "\n";
	if (!header.Read(file))
	{
		std::cerr << "Error parsing kdbx header." << std::endl;
		return false;
	}

	payload.SetHeader(&header);

	// Get composite key
	SecureVector<uint8_t> composite_key;
	if (!composite_key_gen.GetKey(composite_key))
	{
		std::cerr << "Failed to retriev composite key" << std::endl;
		return false;
	}
	payload.SetCompositeKey(composite_key);

	*VerboseStream::Instance() << "\n";
	if (!payload.ReadPayload(file))
	{
		std::cerr << "Error reading encrypted payload." << std::endl;
		return false;
	}

	if (!payload.DecryptPayload())
	{
		std::cerr << "Error decrypting payload" << std::endl;
		return false;
	}

	if (!payload.UnpackPayloadAreas())
	{
		std::cerr << "Failed to unpack payload areas." << std::endl;
		return false;
	}

	payload_xml.SetHeader(&header);
	if (!payload_xml.CheckHeaderHash(payload.payload_blocks))
	{
		std::cerr << "Failed to parse XML." << std::endl;
		return false;
	}

	return true;
}

bool KDBXFile::CompositeKeyAdd(const uint8_t* buf, const size_t len)
{
	if (!composite_key_gen.Add(buf, len))
	{
		std::cerr << "Failed to add data composite key" << std::endl;
		return false;
	}

	return true;
}

bool KDBXFile::CompositeKeyAdd(const SecureVector<uint8_t> &key_part)
{
	if (!composite_key_gen.Add(key_part))
	{
		std::cerr << "Failed to add data composite key" << std::endl;
		return false;
	}

	return true;
}

bool KDBXFile::PrintTree()
{
	return payload_xml.PrintTree(payload.payload_blocks);
}

bool KDBXFile::PrintUUID(const std::string &uuid)
{
	return payload_xml.PrintUUID(payload.payload_blocks, uuid);
}
