#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"
#include "src/format/payload_block.h"
#include "src/format/read.h"
#include "src/crypto/crypto.h"

#include <string.h>


bool PayloadBlock::Read(const SecureVector<uint8_t> &buf_in, size_t &offset)
{
	size_t size = buf_in.size() - offset;
	*DebugStream::Instance() << "Input buffer size = " << size << "\n";

	if (size == 0)
		return false;

	// BlockID
	if (!ReadArray(&buf_in[offset], size, block_id))
	{
		std::cerr << "Buffer is too small to contain full block_id" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Block id: " << block_id << "\n";
	offset += sizeof(block_id);
	size -= sizeof(block_id);

	// Hash
	if (size < sizeof(hash))
	{
		std::cerr << "Buffer (" << size << ") is too small to contain full hash" << std::endl;
		return false;
	}
	memcpy(hash, &buf_in[offset], sizeof(hash));
	*DebugStream::Instance() << "Block hash: 0x" << Buffer(hash, sizeof(hash)) << "\n";
	offset += sizeof(hash);
	size -= sizeof(hash);

	// BlockSize
	if (!ReadArray(&buf_in[offset], size, block_size))
	{
		std::cerr << "Buffer is too small to contain full block_size" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Block size: " << block_size << "\n";
	offset += sizeof(block_size);
	size -= sizeof(block_size);

	// BData
	if (size < block_size)
	{
		std::cerr << "Buffer is too small to contain full block_data. Missing " << block_size - size << " bytes." << std::endl;
		return false;
	}
	block_data.resize(block_size);
	memcpy(block_data.data(), &buf_in[offset], block_size);
	*DebugStream::Instance() << "Block data: 0x" << block_data << "\n";
	offset += block_size;
	size -= block_size;

	return IsValid();
}

bool PayloadBlock::IsValid() const
{
	if (IsLast())
		return true;

	// Check hash
	SecureVector<uint8_t> block_hash;
	Sha256 sha256;
	sha256.Hash(block_data, block_hash);
	*DebugStream::Instance() << "Calculated block data hash = 0x" << block_hash << "\n";

	if (block_hash.size() != sizeof(hash))
	{
		std::cerr << "Hash length doesn't match" << std::endl;
		return false;
	}

	for (size_t i = 0; i < sizeof(hash); i++)
	{
		if (block_hash[i] != hash[i])
			return false;
	}

	return true;
}

bool PayloadBlock::IsLast() const
{
	for (size_t i = 0; i < sizeof(hash); i++)
		if (hash[i] != 0)
			return false;

	if (block_size != 0)
		return false;

	return true;
}
