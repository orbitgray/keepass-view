#include "src/format/kdbx_file_payload.h"
#include "src/format/read.h"
#include "src/format/master_key.h"
#include "src/format/decrypt_payload.h"
#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"
#include "src/crypto/compression.h"


#include <iostream>
#include <assert.h>

KDBXFilePayload::KDBXFilePayload()
	: encrypted_payload_size(0)
	, header(nullptr)
{}

void KDBXFilePayload::SetHeader(KDBXFileHeader *header_in)
{
	assert(header_in);
	header = header_in;
}

void KDBXFilePayload::SetCompositeKey(const SecureVector<uint8_t> &key)
{
	composite_key = key;
}

bool KDBXFilePayload::ReadPayload(std::ifstream &file)
{
	// Figure out payload size
	std::streampos reading_from = file.tellg();

	if (reading_from == -1)
	{
		std::cerr << "Can't get current file position." << std::endl;
		return false;
	}

	file.seekg(0, std::ios::end);
	std::streampos file_size = file.tellg();
	if (file_size == -1)
	{
		std::cerr << "Can't get current file position." << std::endl;
		return false;
	}
	file_size -= reading_from;
	encrypted_payload_size = file_size;
	*DebugStream::Instance() << "Encrypted payload size: " << encrypted_payload_size << " bytes\n";

	// Rewind stream back
	file.seekg(reading_from);

	// Read payload to the buffer.
	encrypted_payload_area.reset(new uint8_t[encrypted_payload_size]);
	if (!::Read(file, encrypted_payload_area.get(), encrypted_payload_size))
		return false;

	return true;
}

bool KDBXFilePayload::DecryptPayload()
{
	// Create master key based on header and user input
	MasterKey master_key( composite_key
						, header->header_entries[KDBXFileHeaderEntry::BId::TRANSFORMSEED].get()
						, header->header_entries[KDBXFileHeaderEntry::BId::TRANSFORMROUNDS].get()
						, header->header_entries[KDBXFileHeaderEntry::BId::MASTERSEED].get()
						);
	if (master_key.master_key_size != 32) // TODO fix this magic number
	{
		std::cerr << "Bad master key size(" << master_key.master_key_size << ") created." << std::endl;
		return false;
	}

	// Decrypt payload
	if (!::DecryptPayload(&encrypted_payload_area[0]
						, encrypted_payload_size
						, plain_payload
						, &master_key.master_key[0]
						, master_key.master_key_size
						, header->header_entries[KDBXFileHeaderEntry::BId::ENCRYPTIONIV].get()
						, header->header_entries[KDBXFileHeaderEntry::BId::STREAMSTARTBYTES].get()
		))
	{
		std::cerr << "Payload decryption has failed." << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Decrypted payload: (" << plain_payload.size() << " - 32b hash) 0x" << Buffer(&plain_payload[32], encrypted_payload_size-32) << "\n";

	return true;
}

bool KDBXFilePayload::UnpackPayloadAreas()
{
	if (!ReadPayloadAreas())
	{
		std::cerr << "Failed to read decrypted payload areas." << std::endl;
		return false;
	}

	if (!DecompressPayloadBlocks())
	{
		std::cerr << "Failed to decompress paylead blocks." << std::endl;
		return false;
	}

	return true;
}

bool KDBXFilePayload::ReadPayloadAreas()
{
	// TODO: move to some header
	// TODO: refactor decrypt payload to use vector, and net return first 32 bytes? or check them here...
	SecureVector<uint8_t> plain_payload_blocks(&plain_payload[header->header_entries[KDBXFileHeaderEntry::BId::STREAMSTARTBYTES]->w_size], &plain_payload[encrypted_payload_size - 1]);
	size_t offset = 0;
	do
	{
		PayloadBlock* payload_block = new PayloadBlock;
		bool res = payload_block->Read(plain_payload_blocks, offset);
		if (!res)
		{
			delete payload_block;
			std::cerr << "PayloadBlock reading has failed" << std::endl;
			return false;
		}
		payload_blocks.push_back(payload_block);

		if (payload_block->IsLast())
			break;
	} while (true);

	return true;
}

bool KDBXFilePayload::DecompressPayloadBlocks()
{
	// TODO: save this value while reading header and reuse here.
	uint32_t compression;
	if (!ReadArray(header->header_entries[KDBXFileHeaderEntry::BId::COMPRESSIONFLAGS]->b_data, compression))
	{
		// should never happen at this point.
		std::cerr << "can't get compression type" << std::endl;
		return false;
	}
	if (compression == KDBXFileHeaderEntry::CompressionType::GZIP_COMPRESSED)
	{
		for (auto it : payload_blocks)
		{
			if (it->IsLast())
				break;

			if (!Compression::Unpack(it->block_data, it->block_data_decompressed))
			{
				std::cerr << "Payload unpacking has failed." << std::endl;
				return false;
			}
			/*
			*DebugStream::Instance() << "XML payload: ";
			for (auto it2 : it->block_data_decompressed)
			{
				*DebugStream::Instance() << (char)(it2);
			}
			*DebugStream::Instance() << "\n";
			*/
		}
	}
	else
	{
		// copy payload as it is it is not compressed.
		for (auto it : payload_blocks)
		{
			if (it->IsLast())
				break;

			it->block_data_decompressed = it->block_data;
		}
	}

	return true;
}
