#ifndef DECRYPT_PAYLOAD_H
#define DECRYPT_PAYLOAD_H

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header.h"

#include <cstdint>

// buf_out should be at least same size as buf_in (size_in)
bool DecryptPayload(uint8_t *buf_in
				, const size_t size_in
				, SecureVector<uint8_t> &buf_out
				, const uint8_t *master_key
				, const size_t master_key_size
				, const KDBXFileHeaderEntry* encryption_iv
				, const KDBXFileHeaderEntry* stream_start_bytes
				);

#endif // DECRYPT_PAYLOAD_H
