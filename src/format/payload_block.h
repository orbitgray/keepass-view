#ifndef PAYLOAD_BLOCK_H
#define PAYLOAD_BLOCK_H

#include "src/common/secure_vector.h"

#include <cstdint>
#include <cstddef>

class PayloadBlock
{
public:
	uint32_t block_id;
	uint8_t hash[32];
	uint32_t block_size;
	SecureVector<uint8_t> block_data;
	SecureVector<uint8_t> block_data_decompressed;

	// Reads block from buf_in. With starting from offset. And updates offset to point to next byte after block.
	bool Read(const SecureVector<uint8_t> &buf_in, size_t &offset);

	// Checks integrity of b_data. (calculates hash and compares against hash[])
	bool IsValid() const;

	// Checks if it is last block
	bool IsLast() const;
};

#endif // PAYLOAD_BLOCK_H
