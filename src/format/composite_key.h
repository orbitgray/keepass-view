#ifndef COMPOSITE_KEY_H
#define COMPOSITE_KEY_H

#include "src/common/secure_vector.h"

#include <cstdint>
#include <cstddef>

class CompositeKey
{
public:
	bool Add(const uint8_t *buf, const size_t size);
	bool Add(const SecureVector<uint8_t> &key_part);
	bool GetKey(SecureVector<uint8_t>& composite_key);

private:
	SecureVector<uint8_t> composite_key_input;
};

#endif
