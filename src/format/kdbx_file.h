#ifndef KDBX_FILE_H
#define KDBX_FILE_H

#include "src/format/kdbx_file_header.h"
#include "src/format/kdbx_file_payload.h"
#include "src/format/kdbx_file_payload_xml.h"
#include "src/format/composite_key.h"

#include <stdint.h>
#include <fstream>

class KDBXFile
{
	KDBXFileHeader header;
	KDBXFilePayload payload;
	KDBXFilePayloadXml payload_xml;

	CompositeKey composite_key_gen;

public:
	KDBXFile();

	bool Read(std::ifstream &file);
	bool CompositeKeyAdd(const uint8_t* buf, const size_t len);
	bool CompositeKeyAdd(const SecureVector<uint8_t> &key_part);

	bool PrintTree();
	bool PrintUUID(const std::string &uuid);
private:
	// bool ParseXML();
};

#endif // KDBX_FILE_H
