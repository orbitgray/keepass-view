#ifndef READ_HELPER_H
#define READ_HELPER_H

#include "src/common/secure_vector.h"

#include <climits>
#include <cstdint>
#include <fstream>

template <class T>
bool ReadArray(const uint8_t *in, size_t size, T &out)
{
	if (size < sizeof(out))
		return false;

	out = 0;
	for (size_t i = 0; i < sizeof(T); i++)
	{
		out |= static_cast<T>(in[i]) << i * CHAR_BIT * sizeof(in[0]);
	}

	return true;
}

template <class T>
bool ReadArray(const SecureVector<uint8_t> &in, T &out)
{
	return ReadArray(in.data(), in.size(), out);
}

bool Read(std::ifstream &file, uint8_t *out, const size_t size);

template <class T>
bool Read(std::ifstream &file, T &out)
{
	T raw_out;
	uint8_t *buf = (uint8_t*)&raw_out;

	bool res = Read(file, buf, sizeof(raw_out));

	if (res)
	{
		res = ReadArray(buf, sizeof(raw_out), out);
	}

	return res;
}

#endif // READ_HELPER_HEADER
