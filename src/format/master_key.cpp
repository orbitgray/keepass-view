#include <iostream>
#include <memory>
#include <string.h>
#include <assert.h>

#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"
#include "src/format/master_key.h"
#include "src/format/read.h"

MasterKey::MasterKey(
	  const SecureVector<uint8_t> &composit_key
	, const KDBXFileHeaderEntry* transformseed
	, const KDBXFileHeaderEntry* transformrounds
	, const KDBXFileHeaderEntry* masterseed
	)
	: composit_key(32, 0) // TODO use consts for SHA256 digest size
{
	assert(composit_key.size() != 0);
	if (composit_key.size() == 0)
	{
		std::cerr << "Expected not empty composite key" << std::endl;
		exit(-1);
	}

	// AES-256-ECB composit context
	assert(sizeof(aes_256_ecb_context.key) == aes256ecb.KeyLength());
	if (sizeof(aes_256_ecb_context.key) != aes256ecb.KeyLength())
	{
		std::cerr << "Expected aes_256_ecb_context.key length (" << sizeof(aes_256_ecb_context.key) << ") doesn't match crypto AES256-ECB key length (" << aes256ecb.KeyLength() << ")" << std::endl;
		exit(-1);
	}

	memset(&aes_256_ecb_context.iv[0], 0, sizeof(aes_256_ecb_context.iv));
	if (transformseed->w_size != sizeof(aes_256_ecb_context.key) || transformseed->b_data.size() != sizeof(aes_256_ecb_context.key))
	{
		std::cerr << "Transform seed size (" << transformseed->w_size
				  << ") doesn't match AES-256-ECB key size (" << sizeof(aes_256_ecb_context.key) << ")"
				  << std::endl;
		exit(-1);
	}
	memcpy(&aes_256_ecb_context.key[0], transformseed->b_data.data(), transformseed->w_size);

	if (transformrounds->w_size != sizeof(aes_256_ecb_context.rounds) || transformrounds->b_data.size() != sizeof(aes_256_ecb_context.rounds))
	{
		std::cerr << "Transform rounds size (" << transformrounds->w_size
				  << ") doesn't match expected value (" << sizeof(aes_256_ecb_context.rounds) << ")"
				  << std::endl;
		exit(-1);
	}
	::ReadArray((uint8_t*)transformrounds->b_data.data(), transformrounds->b_data.size(), aes_256_ecb_context.rounds);
	*DebugStream::Instance() << "Doing " << aes_256_ecb_context.rounds << " rounds of AES-256-ECB" << "\n";

	// Generate transformed key
	SecureVector<uint8_t> transformed_key;
	if (!aes256ecb.Encrypt(composit_key, transformed_key, aes_256_ecb_context))
	{
		std::cerr << "Error ocured, while generating transformed key with AES256ECB" << std::endl;
		exit(-1);
	}
	*DebugStream::Instance() << "Transformed key: 0x" << composit_key << "\n";

	// Generate master key
	// master_key=sha256(CONCAT(MASTERSEED,sha256(transformed_key))).
	master_key_size = sha256.HashLength();
	master_key.reset(new uint8_t[master_key_size]);

	assert(masterseed->w_size == masterseed->b_data.size());
	size_t master_concat_size = masterseed->w_size + composit_key.size();
	std::unique_ptr<uint8_t[]> master_concat(new uint8_t[master_concat_size]);

	// put masterseed
	assert(masterseed->b_data.size() == masterseed->w_size);
	memcpy(&master_concat[0], &masterseed->b_data[0], masterseed->w_size);

	// sha256(transformed_key)
	sha256.Hash(transformed_key, &master_concat[masterseed->w_size]);

	// get master key
	sha256.Hash(&master_concat[0], master_concat_size, &master_key[0]);

	*DebugStream::Instance() << "Master key: 0x" << Buffer(&master_key[0], master_key_size) << "\n";
}

