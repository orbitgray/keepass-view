#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"
#include "src/format/composite_key.h"
#include "src/crypto/crypto.h"

#include <iostream>

// TODO: optimize this, no need to store input
// we just hash it as we add new stuff
bool CompositeKey::Add(const SecureVector<uint8_t> &key_part)
{
	Sha256 sha256;
	if (!sha256.Hash(key_part, composite_key_input))
	{
		std::cerr << "Failed to hash composite key part." << std::endl;
		return false;
	}

	return true;
}

bool CompositeKey::Add(const uint8_t *buf, const size_t size)
{
	Sha256 sha256;
	size_t pos = composite_key_input.size();
	composite_key_input.resize(pos + sha256.HashLength());
	if (!sha256.Hash(buf, size, &composite_key_input[pos]))
	{
		std::cerr << "Failed to hash composite key part." << std::endl;
		return false;
	}

	return true;
}

bool CompositeKey::GetKey(SecureVector<uint8_t>& composite_key)
{
	*DebugStream::Instance() << "Composit key before last hashing (sha256): 0x" << composite_key_input << "\n";

	Sha256 sha256;
	if (!sha256.Hash(composite_key_input, composite_key))
	{
		std::cerr << "Failed to hash composite key part." << std::endl;
		return false;
	}

	return true;
}

