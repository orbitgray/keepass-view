#ifndef MASTER_KEY_H
#define MASTER_KEY_H

#include <memory>

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header_entry.h" // TODO: remove after refartoring to use vector
#include "src/crypto/crypto.h"

class MasterKey
{
	Sha256    sha256;
	Aes256Cbc aes256cbc;
	Aes256Ecb aes256ecb;

public:
	MasterKey(const SecureVector<uint8_t> &composite_key
			, const KDBXFileHeaderEntry* transformseed
			, const KDBXFileHeaderEntry* transformrounds
			, const KDBXFileHeaderEntry* masterseed
			);

	// composite password
	std::unique_ptr<uint8_t[]> password_composit;
	size_t password_composit_length;

	SecureVector<uint8_t> composit_key;

	// master key
	std::unique_ptr<uint8_t[]> master_key;
	size_t master_key_size = 0;

	Aes256::Context aes_256_ecb_context;
	Aes256::Context aes_256_cbc_context;
};

#endif // MASTER_KEY_H
