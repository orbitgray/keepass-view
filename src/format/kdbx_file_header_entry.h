#ifndef KDBX_FILE_HEADER_ENTRY_H
#define KDBX_FILE_HEADER_ENTRY_H

#include "src/common/secure_vector.h"

#include <fstream>
#include <stdint.h>

class KDBXFileHeaderEntry
{
public:
	enum BId : uint8_t
	{
		  END                 =  0
		, COMMENT             =  1
		, CIPHERID            =  2
		, COMPRESSIONFLAGS    =  3
		, MASTERSEED          =  4
		, TRANSFORMSEED       =  5
		, TRANSFORMROUNDS     =  6
		, ENCRYPTIONIV        =  7
		, PROTECTEDSTREAMKEY  =  8
		, STREAMSTARTBYTES    =  9
		, INNERRANDOMSTREAMID = 10
		, MAX_HEADER_ENTRY_ID
	};

	enum CompressionType : uint32_t
	{
		  NOT_COMPRESSED  = 0
		, GZIP_COMPRESSED = 1
		, MAX_VALUE
	};

	enum InnerRandomStreamId : uint32_t
	{
		  NONE                     = 0
		, ARC_4_VARIANT            = 1
		, Salsa20                  = 2
		, MAX_INNER_RANDOM_STREAM
	};

	// BId id;
	uint8_t id;
	uint16_t w_size;
	SecureVector<uint8_t> b_data;


	KDBXFileHeaderEntry();
	KDBXFileHeaderEntry(const BId& id_in);

	~KDBXFileHeaderEntry() {};

	bool Read(std::ifstream &file);

	static std::string TypeToString(const uint8_t type_id);

protected:
	bool ReadId(std::ifstream &file);
	bool ReadWSize(std::ifstream &file);
	bool ReadBData(std::ifstream &file);

	bool Verify(); // this function will do verification based on entry type if possible.
	bool VerifyEND();
	bool VerifyCOMMENT();
	bool VerifyCIPHERID();
	bool VerifyCOMPRESSIONFLAGS();
	bool VerifyMASTERSEED();
	bool VerifyTRANSFORMSEED();
	bool VerifyTRANSFORMROUNDS();
	bool VerifyENCRYPTIONIV();
	bool VerifyPROTECTEDSTREAMKEY();
	bool VerifySTREAMSTARTBYTES();
	bool VerifyINNERRANDOMSTREAMID();
};

#endif // KDBX_FILE_HEADER_ENTRY_H
