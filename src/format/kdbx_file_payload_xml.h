#ifndef KDBX_FILE_PAYLOAD_XML_H
#define KDBX_FILE_PAYLOAD_XML_H

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header.h"
#include "src/format/payload_block.h"
#include "src/format/protected_stream.h"

#include <libxml/parser.h>

#include <string>


class KDBXFilePayloadXml
{
	KDBXFileHeader *header;
	xmlDocPtr doc;

	ProtectedStream protected_stream;

public:
	KDBXFilePayloadXml();
	~KDBXFilePayloadXml();

	SecureVector<PayloadBlock*> payload_blocks;

	void SetHeader(KDBXFileHeader* header);
	bool CheckHeaderHash(const SecureVector<PayloadBlock*> &payload_blocks);
	bool Parse(SecureVector<PayloadBlock*> payload_blocks);
	bool PrintTree(const SecureVector<PayloadBlock*> &payload_blocks);
	bool PrintUUID(const SecureVector<PayloadBlock*> &payload_blocks, const std::string &uuid);
};

#endif // KDBX_FILE_PAYLOAD_XML_H
