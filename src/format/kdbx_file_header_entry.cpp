#include <assert.h>
#include <iostream>

#include "src/common/debug_stream.h"
#include "src/format/kdbx_file_header_entry.h"
#include "src/format/read.h"

KDBXFileHeaderEntry::KDBXFileHeaderEntry()
	: id(MAX_HEADER_ENTRY_ID)
{}

KDBXFileHeaderEntry::KDBXFileHeaderEntry(const BId& id_in)
	: id(id_in)
{
	assert(id < MAX_HEADER_ENTRY_ID);
	if ( !(id < MAX_HEADER_ENTRY_ID) )
	{
		std::cerr << "Unknown header entry type" << std::endl;
	}
}

bool KDBXFileHeaderEntry::ReadId(std::ifstream &file)
{
	bool res = ::Read(file, id);
	if (!res)
	{
		id = MAX_HEADER_ENTRY_ID;
		std::cerr << "Unexpected end of file" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Element ID: " << TypeToString(id) << "\n";

	assert(id < MAX_HEADER_ENTRY_ID);
	if ( !(id < MAX_HEADER_ENTRY_ID) )
	{
		std::cerr << "Unknown header entry type" << std::endl;
		return false;
	}

	return true;
}

bool KDBXFileHeaderEntry::ReadWSize(std::ifstream &file)
{
	bool res = ::Read(file, w_size);
	if (!res)
	{
		w_size = 0;
		std::cerr << "Unexpected end of file" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Element wSize: " << w_size << "\n";

	return true;
}

bool KDBXFileHeaderEntry::ReadBData(std::ifstream &file)
{
	DebugStream::Instance()->SaveFormat();

	b_data.resize(w_size);
	bool res = ::Read(file, b_data.data(), w_size);
	if (!res)
	{
		std::cerr << "Unexpected end of file" << std::endl;
		return false;
	}

	*DebugStream::Instance() << "Element bData: 0x";
	*DebugStream::Instance() << std::setfill('0') << std::setw(2) << std::hex;
	for (size_t i = 0; i < w_size; i++)
	{
		*DebugStream::Instance() << (uint32_t)b_data[i];
	}

	DebugStream::Instance()->LoadFormat();
	*DebugStream::Instance() << "\n";

	return true;
}

bool KDBXFileHeaderEntry::Read(std::ifstream &file)
{
	if (!ReadId(file))
		return false;

	if (!ReadWSize(file))
		return false;

	if (!ReadBData(file))
		return false;

	return Verify();
}

std::string KDBXFileHeaderEntry::TypeToString(const uint8_t type_id)
{
	switch (type_id)
	{
		case END:
			return "END";
		case COMMENT:
			return "COMMENT";
		case CIPHERID:
			return "CIPHERID";
		case COMPRESSIONFLAGS:
			return "COMPRESSIONFLAGS";
		case MASTERSEED:
			return "MASTERSEED";
		case TRANSFORMSEED:
			return "TRANSFORMSEED";
		case TRANSFORMROUNDS:
			return "TRANSFORMROUNDS";
		case ENCRYPTIONIV:
			return "ENCRYPTIONIV";
		case PROTECTEDSTREAMKEY:
			return "PROTECTEDSTREAMKEY";
		case STREAMSTARTBYTES:
			return "STREAMSTARTBYTES";
		case INNERRANDOMSTREAMID:
			return "INNERRANDOMSTREAMID";
		case MAX_HEADER_ENTRY_ID:
		default:
			return "UNKNOWN_ENTRY_ID";
	}
}

bool KDBXFileHeaderEntry::Verify()
{
	switch (id)
	{
		case END:
			return VerifyEND();
		case COMMENT:
			return VerifyCOMMENT();
		case CIPHERID:
			return VerifyCIPHERID();
		case COMPRESSIONFLAGS:
			return VerifyCOMPRESSIONFLAGS();
		case MASTERSEED:
			return VerifyMASTERSEED();
		case TRANSFORMSEED:
			return VerifyTRANSFORMSEED();
		case TRANSFORMROUNDS:
			return VerifyTRANSFORMROUNDS();
		case ENCRYPTIONIV:
			return VerifyENCRYPTIONIV();
		case PROTECTEDSTREAMKEY:
			return VerifyPROTECTEDSTREAMKEY();
		case STREAMSTARTBYTES:
			return VerifySTREAMSTARTBYTES();
		case INNERRANDOMSTREAMID:
			return VerifyINNERRANDOMSTREAMID();
		case MAX_HEADER_ENTRY_ID:
		default:
			return true;
	}
}

bool KDBXFileHeaderEntry::VerifyEND()
{
	// TODO: is it always 4 bytes with some magic?
}

bool KDBXFileHeaderEntry::VerifyCOMMENT()
{
	// TODO
	// try printing it as a string
	*DebugStream::Instance() << "Comment as string: '";
	for (size_t i = 0; i < w_size; i++)
	{
		*DebugStream::Instance() << static_cast<char>(b_data[i]);
	}
	*DebugStream::Instance() << "'" << "\n";
}

bool KDBXFileHeaderEntry::VerifyCIPHERID()
{
	const uint32_t CIPHERID_AES256_W_SIZE = 16;
	const uint8_t CIPHERID_AES256_B_DATA[CIPHERID_AES256_W_SIZE] = {0x31, 0xc1, 0xf2, 0xe6, 0xbf, 0x71, 0x43, 0x50, 0xbe, 0x58, 0x05, 0x21, 0x6a, 0xfc, 0x5a, 0xff};

	// We only support AES256
	if (w_size != CIPHERID_AES256_W_SIZE)
		return false;
	
	for (size_t i = 0; i < CIPHERID_AES256_W_SIZE; i++)
	{
		if (CIPHERID_AES256_B_DATA[i] != b_data[i])
			return false;
	}
	*DebugStream::Instance() << "Cipher: AES256" << "\n";

	return true;
}

bool KDBXFileHeaderEntry::VerifyCOMPRESSIONFLAGS()
{
	if ( w_size != 4 )
	{
		std::cerr << "Expected Compression flags b_data length to be 4 bytes." << std::endl;
		return false;
	}

	// TODO: should we store it? or just add helper to get 4byte values from array?
	uint32_t compression;
	compression = (b_data[0] << 0)
				| (b_data[1] << 8)
				| (b_data[2] << 16)
				| (b_data[3] << 24)
				;
	*DebugStream::Instance() << "Compression type = " << compression << "\n";

	*DebugStream::Instance() << "Compression type: ";
	switch (compression)
	{
		case NOT_COMPRESSED:
			*DebugStream::Instance() << "not compressed";
			break;
		case GZIP_COMPRESSED:
			*DebugStream::Instance() << "gzip compressed";
			break;
		default:
			*DebugStream::Instance() << "unknown compression";
			return false;
	}


	return true;
}

bool KDBXFileHeaderEntry::VerifyMASTERSEED()
{
	const uint32_t MASTERSEED_W_SIZE = 32;

	// Check if length is correct.
	if (w_size != MASTERSEED_W_SIZE)
	{
		std::cerr << "Expected MASTERSEED to be " << MASTERSEED_W_SIZE << " bytes. But got " << w_size << " bytes." << std::endl;
		return false;
	}
	
	return true;
}

bool KDBXFileHeaderEntry::VerifyTRANSFORMSEED()
{
	// should it be more than 0 bytes long?
}

bool KDBXFileHeaderEntry::VerifyTRANSFORMROUNDS()
{
	if ( w_size != 8 )
	{
		std::cerr << "Expected TransformRounds to be 8 bytes long." << std::endl;
		return false;
	}
}

bool KDBXFileHeaderEntry::VerifyENCRYPTIONIV()
{
	return true;
}

bool KDBXFileHeaderEntry::VerifyPROTECTEDSTREAMKEY()
{
	return true;
}

bool KDBXFileHeaderEntry::VerifySTREAMSTARTBYTES()
{
	return true;
}

bool KDBXFileHeaderEntry::VerifyINNERRANDOMSTREAMID()
{
	if ( w_size != 4 )
	{
		std::cerr << "Expected InnerRandomStreamId to be 4 bytes long." << std::endl;
		return false;
	}

	uint32_t inner_random_stream_id;
	if (!ReadArray(b_data.data(), w_size, inner_random_stream_id))
	{
		std::cerr << "Error while extracting INNERRANDOMSTREAMID from b_data." << std::endl;
		return false;
	}
	*DebugStream::Instance() << "Inner random stream ID: " << inner_random_stream_id << "\n";


	*DebugStream::Instance() << "Inner random stream encryption: ";
	switch (inner_random_stream_id)
	{
		case NONE:
			*DebugStream::Instance() << "none";
			break;
		case ARC_4_VARIANT:
			*DebugStream::Instance() << "arg4variant";
			break;
		case Salsa20:
			*DebugStream::Instance() << "salsa20";
			break;
		case MAX_INNER_RANDOM_STREAM:
		default:
			*DebugStream::Instance() << "unknown" << "\n";
			return false;
	}

	return true;
}

