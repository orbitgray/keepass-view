#include "src/format/kdbx_file_payload_xml.h"
#include "src/common/base64.h"
#include "src/common/print_hex.h"
#include "src/common/verbose_stream.h"
#include "src/common/debug_stream.h"
#include "src/common/secure_vector.h"
#include "src/crypto/crypto.h"


#include <libxml/parser.h>
#include <libxml/tree.h>

#include <iostream>
#include <assert.h>

bool is_node_history(xmlDocPtr doc, xmlNode *a_node)
{
	xmlNodePtr cur_node = NULL;

	for (cur_node = a_node; cur_node; cur_node = cur_node->parent)
	{
		if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"History"))
		{
			return true;
		}
	}

	return false;
}

xmlNodePtr
find_node_with_name_no_rec(xmlDocPtr doc, xmlNode *a_node, const std::string &name)
{
	xmlNodePtr cur_node = NULL;

	for (cur_node = a_node; cur_node; cur_node = cur_node->next)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)name.c_str()))
			{
				return cur_node;
			}
		}
	}

	return NULL;
}

static SecureVector<xmlNodePtr>
find_nodes_with_name(xmlDocPtr doc, xmlNode *a_node, const std::string &name)
{
	SecureVector<xmlNodePtr> res;
	xmlNodePtr cur_node = NULL;

	for (cur_node = a_node; cur_node; cur_node = cur_node->next)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)name.c_str()))
			{
				res.push_back(cur_node);
			}
		}

		SecureVector<xmlNodePtr> sub_res(find_nodes_with_name(doc, cur_node->children, name));
		res.insert(res.end(), sub_res.begin(), sub_res.end());
	}

	return res;
}

static void
print_element_names(xmlDocPtr doc, xmlNode *a_node, const size_t level = 0)
{
	xmlNodePtr cur_node = NULL;

	// skip history
	if (is_node_history(doc, a_node))
		return;

	xmlChar *key = NULL;
	xmlChar *key_val = NULL;
	for (cur_node = a_node; cur_node; cur_node = cur_node->next)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			// if ((!xmlStrcmp(cur->name, (const xmlChar *)"storyinfo"))){
			for (size_t i = 0; i < level; i++)
				*VerboseStream::Instance() << "\t";
			*VerboseStream::Instance() << cur_node->name;
			if (   !xmlStrcmp(cur_node->name, (const xmlChar *)"UUID")
				|| !xmlStrcmp(cur_node->name, (const xmlChar *)"Name")
				|| !xmlStrcmp(cur_node->name, (const xmlChar *)"Key")
				|| !xmlStrcmp(cur_node->name, (const xmlChar *)"Value")
				)
			{
				xmlChar *val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
				if (val != NULL)
					*VerboseStream::Instance() << " : " << val;
				xmlFree(val);
			}

			if (!xmlStrcmp(cur_node->name, (const xmlChar *)"Key"))
				key = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);

			if (!xmlStrcmp(cur_node->name, (const xmlChar *)"Value"))
				key_val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);

			*VerboseStream::Instance() << "\n";
		}

		print_element_names(doc, cur_node->children, level + 1);
	}

	// Print key_value if one was found
	if (key != NULL)
	{
		for (size_t i = 0; i < level; i++)
			*VerboseStream::Instance() << "\t";

		*VerboseStream::Instance() << key;

		if (key_val != NULL)
			*VerboseStream::Instance() << " : " << key_val;

		*VerboseStream::Instance() << "\n";
	}
	
	// Releasing values
	if (key != NULL)
		xmlFree(key);

	if (key_val != NULL)
		xmlFree(key_val);
}


// This function will retrieve first Value by Key from 'String' nodes.
static std::string
get_val_by_key(xmlDocPtr doc, xmlNode *a_node, const std::string key_search)
{
	std::string res;

	// Get all key value nodes
	SecureVector<xmlNodePtr> nodes = find_nodes_with_name(doc, a_node, "String");

	// Find title
	for (auto it : nodes)
	{
		xmlChar* key = NULL;
		xmlChar* val = NULL;
		xmlNodePtr cur_node = NULL;

		for (cur_node = it->children; cur_node; cur_node = cur_node->next)
		{
			if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Key"))
				key = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
			if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Value"))
				val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
		}

		if (key != NULL 
				&& 0 == xmlStrcmp(key, (const xmlChar *)key_search.c_str()))
		{
			if (val != NULL)
			{
				// Found our key and value
				res.append((char*)val);
			}

			// cleanup
			if (key != NULL)
				xmlFree(key);
			if (val != NULL && val[0] != '\0')
				xmlFree(val);
			break;
		}

		// cleanup
		if (key != NULL)
			xmlFree(key);
		if (val != NULL && val[0] != '\0')
			xmlFree(val);
		key = NULL;
		val = NULL;
	}

	return res;
}

static void
print_entry(xmlDocPtr doc, xmlNode *a_node, const size_t level = 0)
{
		// print title : uuid pair if we have one.
		for (size_t i = 0; i < level; i++)
			std::cout << "\t";

		xmlChar* uuid = xmlNodeListGetString(doc, a_node->xmlChildrenNode, 1);

		// Get all key value nodes
		SecureVector<xmlNodePtr> nodes = find_nodes_with_name(doc, a_node, "String");

		// Find title
		xmlChar* key = NULL;
		xmlChar* val = NULL;
		for (auto it : nodes)
		{
			xmlNodePtr cur_node = NULL;

			for (cur_node = it->children; cur_node; cur_node = cur_node->next)
			{
				if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Key"))
					key = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);                                                             
				if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Value"))
					val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);                                                             
			}

			if (key != NULL 
				&& 0 == xmlStrcmp(key, (const xmlChar *)"Title"))
			{
				if (val != NULL)
				{
					// Found our Title
					break;
				}
			}

			// cleanup
			if (key != NULL)
				xmlFree(key);
			if (val != NULL)
				xmlFree(val);
			key = NULL;
			val = NULL;
		}

		if (val != NULL)
			std::cout << val << std::endl;
		else
			std::cout << "<No Title>" << std::endl;

		// cleanup
		if (key != NULL)
			xmlFree(key);
		if (val != NULL)
			xmlFree(val);
		key = NULL;
		val = NULL;

		// Print info about this entry
		// UUID
		for (size_t i = 0; i < level + 1; i++)
			std::cout << "\t";
		std::cout << "UUID : " << uuid << std::endl;
		// Everything else
		for (auto it : nodes)
		{
			if (is_node_history(doc, it))
				continue;

			xmlChar* key = NULL;
			xmlChar* val = NULL;
			xmlNodePtr cur_node = NULL;

			for (cur_node = it->children; cur_node; cur_node = cur_node->next)
			{
				if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Key"))
					key = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);                                                             
				if (0 == xmlStrcmp(cur_node->name, (const xmlChar *)"Value"))
					val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);                                                             
			}

			if (key != NULL && val != NULL
				&& (false
					|| 0 == xmlStrcmp(key, (const xmlChar *)"URL")
					|| 0 == xmlStrcmp(key, (const xmlChar *)"UserName")
					// || 0 == xmlStrcmp(key, (const xmlChar *)"Password")
					)
			   )
			{
				for (size_t i = 0; i < level + 1; i++)
					std::cout << "\t";
				std::cout << key << " : " << val << std::endl;
			}

			if (key != NULL && val != NULL
				&& (false
					|| 0 == xmlStrcmp(key, (const xmlChar *)"Title")
					|| 0 == xmlStrcmp(key, (const xmlChar *)"Notes")
					)
				)
			{
				for (size_t i = 0; i < level + 1; i++)
					*VerboseStream::Instance() << "\t";
				*VerboseStream::Instance() << key << " : " << val << "\n";
			}


			// cleanup
			if (key != NULL)
				xmlFree(key);
			if (val != NULL)
				xmlFree(val);
			key = NULL;
			val = NULL;
		}



		xmlFree(uuid);
}

static void
print_element_tree(xmlDocPtr doc, xmlNode *a_node, const size_t level = 0)
{
	xmlNodePtr cur_node = NULL;

	int level_delta = 0;

	// skip history
	if (is_node_history(doc, a_node))
		return;

	// Print group / element name
	xmlNodePtr uuid_node = find_node_with_name_no_rec(doc, a_node, "UUID");
	if (uuid_node != NULL)
	{
		xmlChar* uuid = xmlNodeListGetString(doc, uuid_node->xmlChildrenNode, 1);
		level_delta = 1;

		xmlNodePtr name_node = find_node_with_name_no_rec(doc, a_node, "Name");
		if (name_node != NULL)
		{
			// this means we have group name.
			xmlChar* name = xmlNodeListGetString(doc, name_node->xmlChildrenNode, 1);

			assert(name != NULL && uuid != NULL);
			if (name == NULL || uuid == NULL)
			{
				std::cerr << "Bad xml node with no string for UUID or Name." << std::endl; 
				return;
			}

			// print name : uuid pair if we have one.
			for (size_t i = 0; i < level; i++)
				std::cout << "\t";

			// std::cout << name << " : " << uuid << std::endl;
			std::cout << name << std::endl;

			// cleanup
			if (name)
				xmlFree(name);
		} else {
			// if 'Name' is missing this means it is entry for a group.
			// Every entry in group will start from 'Entry'
			// We would need to find title from every entry to print it.

			print_entry(doc, uuid_node, level);
		}

		// cleanup


		if (uuid)
			xmlFree(uuid);

		uuid = NULL;
		uuid_node = NULL;
		name_node = NULL;
	}

	// Collect key - value pairs
	xmlChar *key = NULL;
	xmlChar *key_val = NULL;
	for (cur_node = a_node; cur_node; cur_node = cur_node->next)
	{
		if (cur_node->type == XML_ELEMENT_NODE)
		{
			if (!xmlStrcmp(cur_node->name, (const xmlChar *)"Key"))
				key = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);

			if (!xmlStrcmp(cur_node->name, (const xmlChar *)"Value"))
				key_val = xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
		}

		print_element_tree(doc, cur_node->children, level + level_delta);
	}

	// Print key_value if one was found
	if (0 && key != NULL)
	{
		for (size_t i = 0; i < level; i++)
			std::cout << "\t";

		std::cout << key;

		if (key_val != NULL)
			std::cout << " : " << key_val;

		std::cout << std::endl;
	}
	
	// Releasing values
	if (key != NULL)
		xmlFree(key);

	if (key_val != NULL)
		xmlFree(key_val);
}

KDBXFilePayloadXml::KDBXFilePayloadXml()
	: header(nullptr)
	, doc(nullptr)
{
}

KDBXFilePayloadXml::~KDBXFilePayloadXml()
{
	if (doc)
		xmlFreeDoc(doc);
}

void KDBXFilePayloadXml::SetHeader(KDBXFileHeader *header_in)
{
	assert(header_in);
	header = header_in;

	protected_stream.SetHeader(header);
}

bool KDBXFilePayloadXml::CheckHeaderHash(const SecureVector<PayloadBlock*> &payload_blocks)
{
	// Read XML
	*DebugStream::Instance() << "Parsing XML body and checking hashes...\n";
	for (auto it : payload_blocks)
	{
		if (it->IsLast())
			break;

		doc = xmlReadMemory((char*)it->block_data_decompressed.data(), it->block_data_decompressed.size(), "noname.xml", NULL, 0);
		if (doc == NULL)
		{
			std::cerr << "Failed to parse document" << std::endl;
			return false;
		}

		xmlNode *root_element = xmlDocGetRootElement(doc);
		
		// Check Header hash
		{
			// Get hash string
			SecureVector<xmlNodePtr> hash_vec = find_nodes_with_name(doc, root_element, "HeaderHash");
			if (hash_vec.size() != 1)
			{
				std::cerr << "Expected only 1 HeaderHash element in XML. Got " << hash_vec.size() << std::endl;
				return false;
			}
			xmlNodePtr header_hash = hash_vec.back();
			{
				xmlChar *val = xmlNodeListGetString(doc, header_hash->xmlChildrenNode, 1);
				*DebugStream::Instance() << "HeaderHash = " << val << "\n";

				// decode base64 hash
				SecureVector<uint8_t> header_hash_bin = Base64::Decode(reinterpret_cast<char*>(val));
				*DebugStream::Instance() << "HeaderHash bin = 0x" << header_hash_bin << "\n";

				// calc hash
				Sha256 sha256;
				SecureVector<uint8_t> calc_hash;
				if (!sha256.Hash(header->raw_header, calc_hash))
				{
					std::cerr << "Hash calculation has failed" << std::endl;
					return false;
				}
				*DebugStream::Instance() << "Calculated hash = 0x" << calc_hash << "\n";

				// Check if hashes match
				if (calc_hash != header_hash_bin)
				{
					std::cerr << "Header hash missmatches calculated one." << std::endl;
					return false;
				}

				xmlFree(val);
			}
		}
	}
	*DebugStream::Instance() << "done" << "\n";

	return true;
}
	
bool KDBXFilePayloadXml::PrintTree(const SecureVector<PayloadBlock*> &payload_blocks)
{
	// Read XML
	*DebugStream::Instance() << "Parsing XML body and print tree...\n";
	for (auto it : payload_blocks)
	{
		if (it->IsLast())
			break;

		doc = xmlReadMemory((char*)it->block_data_decompressed.data(), it->block_data_decompressed.size(), "noname.xml", NULL, 0);
		if (doc == NULL)
		{
			std::cerr << "Failed to parse document" << std::endl;
			return false;
		}

		xmlNode *root_element = xmlDocGetRootElement(doc);
		
		// Print tree
		print_element_names(doc, root_element);
		print_element_tree(doc, root_element);
	}
	*DebugStream::Instance() << "done" << "\n";

	return true;
}

bool KDBXFilePayloadXml::PrintUUID(const SecureVector<PayloadBlock*> &payload_blocks, const std::string &uuid_find)
{
	bool found = false;

	// Read XML
	*DebugStream::Instance() << "Parsing XML body and looking for specifit UUID (" <<  uuid_find << ")...\n";
	for (auto it : payload_blocks)
	{
		if (it->IsLast())
			break;

		doc = xmlReadMemory((char*)it->block_data_decompressed.data(), it->block_data_decompressed.size(), "noname.xml", NULL, 0);
		if (doc == NULL)
		{
			std::cerr << "Failed to parse document" << std::endl;
			return false;
		}

		xmlNode *root_element = xmlDocGetRootElement(doc);

		// Loop thourgh all 'Entries' and memories password lengths.
		// We need this because passwords encrypted as 1 stream.
		// Also look for our UUID
		SecureVector<xmlNodePtr> entry_vec = find_nodes_with_name(doc, root_element, "Entry");
		*DebugStream::Instance() << "Found " << entry_vec.size() << " entries\n";
		for (auto& it : entry_vec)
		{
			xmlNodePtr uuid_node = find_node_with_name_no_rec(doc, it->children, "UUID");
			std::string password = get_val_by_key(doc, it, "Password");


			xmlChar* uuid = xmlNodeListGetString(doc, uuid_node->xmlChildrenNode, 1);
			*DebugStream::Instance() << "Lookin at UUID = '" << uuid << "'\n";

			if (0 != xmlStrcmp(uuid, (const xmlChar *)uuid_find.c_str()))
			{
				// Skip entry but remember password length.
				protected_stream.SkipBlock(Base64::Decode(password.c_str()).size());
				// std::cout << "Password : " << std::string((char*)plain_block.data(), plain_block.size()) << std::endl;
			}
			else
			{
				// This is our entry decrypt password
				found = true;
				SecureVector<uint8_t> plain_block = protected_stream.DecryptBlock(Base64::Decode(password.c_str()));
				std::cout << "Password : " << std::string((char*)plain_block.data(), plain_block.size()) << std::endl;
			}

			xmlFree(uuid);

			if (found)
				break;
		}
	}
	*DebugStream::Instance() << "done" << "\n";

	if (!found)
	{
		std::cerr << "Entry with provided UUID was not found. Check if it was entered correctly." << std::endl;
	}

	return found;
}
