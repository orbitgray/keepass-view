#ifndef KDBX_FILE_HEADER_H
#define KDBX_FILE_HEADER_H

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header_entry.h"

#include <fstream>
#include <memory>
#include <stdint.h>

class KDBXFileHeader
{
	static const uint8_t PRIMARY_IDENTIFIER[4];
	static const uint8_t SECONDARY_IDENTIFIER[4];
	static const uint8_t FILE_VERSION_LATEST = 0x67;
	static const uint8_t FILE_VERSION_KEEPASS2_PRE_RELEASE = 0x66;
	static const uint8_t FILE_VERSION_KEEPASS1 = 0x55;

public:
	KDBXFileHeader();

	uint8_t  primary_identifier[4];
	uint8_t  secondary_identifier[4];
	uint8_t  file_version;
	uint16_t file_version_minor;
	uint16_t file_version_major;

	std::unique_ptr<KDBXFileHeaderEntry> header_entries[KDBXFileHeaderEntry::MAX_HEADER_ENTRY_ID];
	SecureVector<uint8_t> raw_header;
	SecureVector<uint8_t> encrypted_payload_area;
	bool Read(std::ifstream &file);

protected:
	bool ReadPrimaryIdentifier(std::ifstream &file);
	bool ReadSecondaryIdentifier(std::ifstream &file);
	bool VerifyFileVersion();
	bool ReadFileVersionMinor(std::ifstream &file);
	bool ReadFileVersionMajor(std::ifstream &file);
	bool ReadHeaderEntries(std::ifstream &file);
	bool ReadRawHeader(std::ifstream &file);
};

#endif // KDBX_FILE_HEADER_H
