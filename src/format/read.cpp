#include "src/format/read.h"

bool Read(std::ifstream &file, uint8_t *out, const size_t size)
{
	file.read((char*)out, size);

	if (!file)
	{
		return false;
	}

	return true;
}
