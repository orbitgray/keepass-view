#include "src/format/decrypt_payload.h"
#include "src/crypto/crypto.h"
#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"

#include <assert.h>
#include <iostream>
#include <memory.h>

bool DecryptPayload(uint8_t *buf_in
				, const size_t size_in
				, SecureVector<uint8_t> &buf_out
				, const uint8_t *master_key
				, const size_t master_key_size
				, const KDBXFileHeaderEntry* encryption_iv
				, const KDBXFileHeaderEntry* stream_start_bytes
				)
{
	assert(buf_in);
	if (buf_in == nullptr)
	{
		std::cerr << "NULL pointer provided instead of valid buf_in pointer" << std::endl;
		return false;
	}

	// Set context
	Aes256::Context aes_256_cbc_context;

	assert(encryption_iv->b_data.size() == encryption_iv->w_size);
	if (encryption_iv->w_size != sizeof(aes_256_cbc_context.iv))
	{
		std::cerr << "Bad size of ENCRYPTIONIV (" << encryption_iv->w_size << "). Expected it to be " << sizeof(aes_256_cbc_context.iv) << std::endl;
		return false;
	}
	memcpy(&aes_256_cbc_context.iv[0], &encryption_iv->b_data[0], encryption_iv->w_size);

	if (master_key_size != sizeof(aes_256_cbc_context.key))
	{
		std::cerr << "Bad size of master key (" << master_key_size << "). Expected it to be " << sizeof(aes_256_cbc_context.key) << std::endl;
		return false;
	}
	memcpy(&aes_256_cbc_context.key[0], &master_key[0], master_key_size);

	aes_256_cbc_context.rounds = 1;

	// Decrypt
	Aes256Cbc aes_256_cbc;
	buf_out.resize(size_in);
	if (!aes_256_cbc.Decrypt(buf_in, size_in, buf_out.data(), buf_out.size(), aes_256_cbc_context))
	{
		std::cerr << "Payload decryption has failed." << std::endl;
		return false;
	}

	// Verify decryption
	assert(stream_start_bytes->b_data.size() == stream_start_bytes->w_size);
	if (0 != memcmp(&stream_start_bytes->b_data[0], buf_out.data(), stream_start_bytes->w_size))
	{
		std::cerr << "Decrypted block doesn't match stream_start_bytes providen in header." << std::endl;
		std::cerr << "Expected: 0x" << Buffer(&stream_start_bytes->b_data[0], static_cast<size_t>(stream_start_bytes->w_size)) << std::endl;
		std::cerr << "Got:      0x" << Buffer(buf_out.data(), stream_start_bytes->w_size) << std::endl;
		return false;
	}
	else
	{
		*DebugStream::Instance() << "Successfully decrypted block that matches stream_start_bytes providen in header.\n";
		*DebugStream::Instance() << "Expected: 0x" << Buffer(&stream_start_bytes->b_data[0], stream_start_bytes->w_size) << "\n";
		*DebugStream::Instance() << "Got:      0x" << Buffer(buf_out.data(), stream_start_bytes->w_size) << "\n";

		// tmp
		// print decrypted payload; checking for paddig
		*DebugStream::Instance() << "Encrypted payload = 0x" << Buffer(buf_in, size_in) << "\n";
		*DebugStream::Instance() << "Decrypted payload = 0x" << buf_out << "\n";
	}

	return true;
}
