#include "src/format/protected_stream.h"
#include "src/common/print_hex.h"
#include "src/common/debug_stream.h"

#include <assert.h>
#include <iostream>

void ProtectedStream::SetHeader(KDBXFileHeader* header_in)
{
	assert(header_in);
	header = header_in;

	InitCypher();
}

void ProtectedStream::InitCypher()
{
	Salsa20::Context context;
	context.iv = IV;

	if (!sha256.Hash(header->header_entries[KDBXFileHeaderEntry::BId::PROTECTEDSTREAMKEY]->b_data, context.key))
	{
		std::cerr << "Error calculating key for Salsa20. Sha256 has failed." << std::endl;
	}
	
	if (!cipher.SetContext(context))
	{
		std::cerr << "Error ocured during Salsa20 initialization from ProtectedStream" << std::endl;
	}
}

void ProtectedStream::SkipBlock(const size_t bytes)
{
	*DebugStream::Instance() << "Protected Stream SkipBlock(" << bytes << ")\n";

	// Decrypt protected stream
	SecureVector<uint8_t> dummy_input(bytes);
	SecureVector<uint8_t> plain_block(bytes);
	if (!cipher.Decrypt(dummy_input, plain_block))
	{
		std::cerr << "Failed decrypting protected stream block" << std::endl;
		return;
	}
}

SecureVector<uint8_t> ProtectedStream::DecryptBlock(const SecureVector<uint8_t>& data)
{
	*DebugStream::Instance() << "Protected Stream DecryptBlock(" << data << ")\n";

	// Decrypt protected stream
	SecureVector<uint8_t> plain_block;
	if (!cipher.Decrypt(data, plain_block))
	{
		std::cerr << "Failed decrypting protected stream block" << std::endl;
		return plain_block;
	}

	*DebugStream::Instance() << "Decoded Protected block hex = '" << plain_block << "'\n";
	*DebugStream::Instance() << "Decoded Protected block = '" << std::string((char*)plain_block.data(), (char*)plain_block.data() + plain_block.size())  << "'\n";

	return plain_block;
}

