#ifndef KDBX_FILE_PAYLOAD_H
#define KDBX_FILE_PAYLOAD_H

#include "src/common/secure_vector.h"
#include "src/format/kdbx_file_header.h"
#include "src/format/payload_block.h"

#include <stdint.h>
#include <fstream>

class KDBXFilePayload
{
	std::unique_ptr<uint8_t[]> encrypted_payload_area;
	size_t encrypted_payload_size;
	SecureVector<uint8_t> plain_payload;

	// TODO: maybe just use map of header entries
	KDBXFileHeader *header;
	SecureVector<uint8_t> composite_key;

public:

	KDBXFilePayload();

	void SetHeader(KDBXFileHeader* header);
	void SetCompositeKey(const SecureVector<uint8_t> &key);

	bool ReadPayload(std::ifstream &file);
	bool DecryptPayload();

	bool UnpackPayloadAreas();

	SecureVector<PayloadBlock*> payload_blocks;

protected:

	bool ReadPayloadAreas();
	bool DecompressPayloadBlocks();
};

#endif // KDBX_FILE_PAYLOAD_H
