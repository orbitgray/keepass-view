#include <assert.h>
#include <iostream>

#include "src/common/debug_stream.h"
#include "src/format/kdbx_file_header.h"
#include "src/format/read.h"

KDBXFileHeader::KDBXFileHeader()
	: primary_identifier{0}
	, secondary_identifier{0}
	, file_version(0)
	, file_version_minor(0)
	, file_version_major(0)
{}

const uint8_t KDBXFileHeader::PRIMARY_IDENTIFIER[4]   = {0x03, 0xD9, 0xA2, 0x9A};
const uint8_t KDBXFileHeader::SECONDARY_IDENTIFIER[4] = {0x67, 0xFB, 0x4B, 0xB5}; // TODO: check if 1-3 changes.

bool KDBXFileHeader::Read(std::ifstream &file)
{
	if (!ReadPrimaryIdentifier(file))
		return false;

	if (!ReadSecondaryIdentifier(file))
		return false;

	if (!ReadFileVersionMinor(file))
		return false;

	if (!ReadFileVersionMajor(file))
		return false;

	if (!ReadHeaderEntries(file))
		return false;

	if (!ReadRawHeader(file))
		return false;

	return true;
}

bool KDBXFileHeader::ReadPrimaryIdentifier(std::ifstream &file)
{
	DebugStream::Instance()->SaveFormat();

	// Read primary identifier
	if (!::Read(file, &primary_identifier[0], sizeof(primary_identifier)))
	{
		std::cerr << "Erorr reading 'primary identifier' from file" << std::endl;
		return false;
	}

	*DebugStream::Instance() << "Primary identifier: ";
	for (size_t i = 0; i < 4; i++)
	{
		*DebugStream::Instance() << "0x" << std::setfill('0') << std::setw(2) << std::hex << (uint32_t)primary_identifier[i] << ", ";
	}
	DebugStream::Instance()->LoadFormat();
	*DebugStream::Instance() << "\n";

	for (size_t i = 0; i < 4; i++)
	{
		if (primary_identifier[i] != PRIMARY_IDENTIFIER[i])
		{
			std::cerr << "Flie primary identifiel doesn't match magic numbers." << std::endl;
			return false;
		}
	}

	return true;
}

bool KDBXFileHeader::ReadSecondaryIdentifier(std::ifstream &file)
{
	DebugStream::Instance()->SaveFormat();

	// Read secondary identifier
	if (!::Read(file, &secondary_identifier[0], sizeof(secondary_identifier)))
	{
		std::cerr << "Erorr reading 'secondary identifier' from file" << std::endl;
		return false;
	}

	*DebugStream::Instance() << "Secondary identifier: ";
	for (size_t i = 0; i < 4; i++)
	{
		*DebugStream::Instance() << "0x" << std::setfill('0') << std::setw(2) << std::hex << (uint32_t)secondary_identifier[i] << ", ";
	}
	DebugStream::Instance()->LoadFormat();

	for (size_t i = 0; i < 4; i++)
	{
		if (secondary_identifier[i] != secondary_identifier[i])
		{
			std::cerr << "Flie secondary identifiel doesn't match magic numbers. (or supported version)" << std::endl;
			return false;
		}
	}
	*DebugStream::Instance() << "\n";

	// File version
	if (!ReadArray(secondary_identifier, sizeof(file_version), file_version))
		return false;

	if (!VerifyFileVersion())
		return false;

	return true;
}

bool KDBXFileHeader::VerifyFileVersion()
{
	DebugStream::Instance()->SaveFormat();
	*DebugStream::Instance() << "File version: 0x" << std::setfill('0') << std::setw(2) << std::hex << (uint32_t)file_version << "\n";
	DebugStream::Instance()->LoadFormat();

	switch (file_version)
	{
		case FILE_VERSION_LATEST:
			// good version we can read;
			break;
		case FILE_VERSION_KEEPASS2_PRE_RELEASE:
			std::cerr << "Looks like KeePass2_pre_release file format. Sorry we don't support it." << std::endl;
			return false;
		case FILE_VERSION_KEEPASS1:
			std::cerr << "Looks like KeePass1 file format. Sorry we don't support it." << std::endl;
			return false;
		default:
			return false;
	}

	return true;
}

bool KDBXFileHeader::ReadFileVersionMinor(std::ifstream &file)
{
	// File version minor
	if (!::Read(file, file_version_minor))
	{
		std::cerr << "Erorr reading 'file minor version' from file" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "File minor version: " << file_version_minor << "\n";

	return true;
}

bool KDBXFileHeader::ReadFileVersionMajor(std::ifstream &file)
{
	// File version major
	if (!::Read(file, file_version_major))
	{
		std::cerr << "Erorr reading 'file major version' from file" << std::endl;
		return false;
	}
	*DebugStream::Instance() << "File major version: " << file_version_major << "\n";

	return true;
}

bool KDBXFileHeader::ReadHeaderEntries(std::ifstream &file)
{
	*DebugStream::Instance() << "\n";

	// Read header entries
	std::unique_ptr<KDBXFileHeaderEntry> entry(new KDBXFileHeaderEntry);
	while (entry->Read(file))
	{
		// Add new line after each element details were printed.
		*DebugStream::Instance() << "\n";

		assert(entry->id < KDBXFileHeaderEntry::BId::MAX_HEADER_ENTRY_ID);
		header_entries[entry->id].swap(entry);

		entry.reset(new KDBXFileHeaderEntry);

		// Check if this is last entry
		if (header_entries[KDBXFileHeaderEntry::BId::END])
			break;
	}

	// Check if we have reached END or there was an error.
	if (!header_entries[KDBXFileHeaderEntry::BId::END])
	{
		std::cerr << "Error occur while reading kdbx header" << std::endl;
		return false;
	}

	return true;
}

bool KDBXFileHeader::ReadRawHeader(std::ifstream &file)
{
	// Store raw header vector for hash check later on.
	std::streampos header_size = file.tellg();
	if (header_size == -1)
	{
		std::cerr << "Can't get current file position." << std::endl;
		return false;
	}
	raw_header.resize(header_size);

	// rewind stream to the beginning
	file.seekg(0, std::ios::beg);

	// copy data
	if (!::Read(file, raw_header.data(), raw_header.size()))
	{
		std::cerr << "Failed to read raw header" << std::endl;
		return false;
	}

	// rewind stream to the same position.
	file.seekg(header_size, std::ios::beg);

	return true;
}

