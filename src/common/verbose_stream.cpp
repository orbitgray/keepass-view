#include "./src/common/verbose_stream.h"


VerboseStream* VerboseStream::singleton_instance = nullptr;

VerboseStream* VerboseStream::Instance()
{
	if (!singleton_instance)
		singleton_instance = new VerboseStream();

	return singleton_instance;
}

void VerboseStream::TurnOn()
{
	turned_on = true;
}

void VerboseStream::TurnOff()
{
	turned_on = false;
}

void VerboseStream::SaveFormat()
{
	state.copyfmt(std::cout);
}

void VerboseStream::LoadFormat()
{
	std::cout.copyfmt(state);
}

