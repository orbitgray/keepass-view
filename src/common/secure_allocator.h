#ifndef SECURE_ALLOCATOR_H
#define SECURE_ALLOCATOR_H

#include <string.h>
#include <memory>

template <typename T>
struct free_zero_allocator
{
	using value_type = T;

	free_zero_allocator() = default;
	free_zero_allocator(const free_zero_allocator&) = default;

	T* allocate(std::size_t n)
	{
		if (auto ptr = std::malloc(n * sizeof(T)))
			return static_cast<T*>(ptr);

		throw std::bad_alloc();
	};

	void deallocate(T* p, size_t n)
	{
		// Zero data before doing standard
		memset(p, 0, n * sizeof(T));

		// This is trickery to stop compiler from optimizing out memset.
		// Since we free the memory and never use it after memset. Compiler is allowed :( to ignore this call
		// Assuming there are no side effects for programm.
		// But in our case this memset is the point of custom allocator.
		// Touching last element just in case of future optimizations. But tmp1 should be enough.
		volatile uint8_t tmp1 = ((uint8_t*)p)[0];
		volatile uint8_t tmp2 = ((uint8_t*)p)[n*sizeof(T)-1];

		// Do the usual deallocation
		std::free(p);
	}
};

template <typename T, typename U>
inline bool operator == (const free_zero_allocator<T>&, const free_zero_allocator<U>&) {
	  return sizeof(T) == sizeof(U);
}

template <typename T, typename U>
inline bool operator != (const free_zero_allocator<T>& a, const free_zero_allocator<U>& b) {
	  return !(a == b);
}

#endif // SECURE_ALLOCATOR_H
