#ifndef DEBUG_STREAM_H
#define DEBUG_STREAM_H

#include <iostream>
#include <iomanip>

// Singleton class for debug output stream
class DebugStream
{
public:
	void TurnOn();
	void TurnOff();

	static DebugStream* Instance();

	template<typename T>
	DebugStream& operator<<(T x)
	{
		if (turned_on)
			std::cout << x;
		return *this;
	}

	void SaveFormat();
	void LoadFormat();

private:
	DebugStream() : state(nullptr) {}

	bool turned_on = false;
	static DebugStream* singleton_instance;

	// For format save/load
	std::ios state;
};

#endif // DEBUG_STREAM_H
