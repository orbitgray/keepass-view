#ifndef BASE64_H
#define BASE64_H

#include <stdlib.h>
#include <cstdint>

#include "./src/common/secure_vector.h"

class Base64
{
public:
	static SecureVector<uint8_t> Decode(const char* buf);

protected:
	// Number of symbols in one block.
	// Base64 string should be multible of blocks.
	static const size_t BASE64_BLOCK_SIZE;

	static const char BASE64_PADDING;
	static const char BASE64_SPACIAL_1;
	static const char BASE64_SPACIAL_2;

	static uint8_t CharToVal(const char c);
	static size_t BlockPadLength(const char* s);
	static SecureVector<uint8_t> CharBlockToVal(const char* s);

};

#endif // BASE64_H
