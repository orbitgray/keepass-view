#ifndef PRINT_HEX_H
#define PRINT_HEX_H

#include "src/common/secure_vector.h"

#include <stdlib.h>
#include <cstdint>
#include <iostream>

std::ostream& operator<<(std::ostream& os, const SecureVector<uint8_t> &buf);

class Buffer
{
public:
	Buffer(const uint8_t *buf_in, const size_t size_in);

	friend std::ostream& operator<<(std::ostream& os, const Buffer& buf);

protected:
	const uint8_t* data = nullptr;
	size_t   size = 0;
};


void PrintHexBuffer(const uint8_t *buf, const size_t size);
void PrintHexBuffer(const SecureVector<uint8_t> &buf);

#endif // PRINT_HEX_H
