#include "./src/common/debug_stream.h"


DebugStream* DebugStream::singleton_instance = nullptr;

DebugStream* DebugStream::Instance()
{
	if (!singleton_instance)
		singleton_instance = new DebugStream();

	return singleton_instance;
}

void DebugStream::TurnOn()
{
	turned_on = true;
}

void DebugStream::TurnOff()
{
	turned_on = false;
}

void DebugStream::SaveFormat()
{
	state.copyfmt(std::cout);
}

void DebugStream::LoadFormat()
{
	std::cout.copyfmt(state);
}

