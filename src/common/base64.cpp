#include "src/common/base64.h"

#include <assert.h>
#include <cstring>
#include <iostream>

const size_t Base64::BASE64_BLOCK_SIZE = 4;

const char Base64::BASE64_PADDING   = '=';
const char Base64::BASE64_SPACIAL_1 = '+';
const char Base64::BASE64_SPACIAL_2 = '/';

// assumes character is valid Base64 symbol.
uint8_t Base64::CharToVal(const char c)
{
	uint8_t res = 0;

	if (c >= 'A' && c <= 'Z')
		res = (0) + c - 'A';
	else if (c >= 'a' && c <= 'z')
		res = ('Z' - 'A' + 1) + c - 'a';
	else if (c >= '0' && c <= '9')
		res = (('Z' - 'A' + 1) + ('z' - 'a' + 1)) + c - '0';
	else if (c == BASE64_SPACIAL_1)
		res = ('Z' - 'A' + 1) + ('z' - 'a' + 1) + ('9' - '0' + 1) + 0;
	else if (c == BASE64_SPACIAL_2)
		res = ('Z' - 'A' + 1) + ('z' - 'a' + 1) + ('9' - '0' + 1) + 1;
	else if (c == BASE64_PADDING)
		res = 0;
	else
	{
		assert(!"Found not Base64 character.");
		return 0;
	}

	return res;
}

size_t Base64::BlockPadLength(const char* s)
{
	assert(strlen(s) >= 4);

	size_t res = 0;

	for (int i = BASE64_BLOCK_SIZE - 1; (i > 0) && (s[i] == BASE64_PADDING); i--, res++)
		;

	// Sanity check. If we have > 3 than input block is invalid
	assert(res < 3);
	if (res >= 3)
		return 0;

	return res;
}

// @todo: optimization: use constexpr to fill out lookup table instead of calling CharToVall all the time.
// This function expects at least BASE64_BLOCK_SIZE valid characters in input.
// And convert them to 3 bytes (3 x uint8_t)
SecureVector<uint8_t> Base64::CharBlockToVal(const char* s)
{
	SecureVector<uint8_t> res(3, 0);
	uint32_t tmp = 0;

	if (strlen(s) < 4)
	{
		assert(!"Base64 block is shorter than expected");
		return res;
	}

	for (size_t i = 0; i < BASE64_BLOCK_SIZE; i++)
	{
		tmp <<= 6;
		tmp |= CharToVal(s[i]);
	}

	for (size_t i = 0; i < 3; i++)
	{
		res[i] = (tmp >> (8 * (2 - i))) & 0xff;
	}

	// remove padded bytes.
	res.resize(3 - BlockPadLength(s));

	return res;
}

SecureVector<uint8_t> Base64::Decode(const char* buf)
{
	SecureVector<uint8_t> res;

	size_t len = strlen(buf);
	assert((len % BASE64_BLOCK_SIZE) == 0);
	if ((len % BASE64_BLOCK_SIZE) != 0)
	{
		std::cerr << "Malformed base64 string: '" << buf << "'" << std::endl;
		exit(-1);
	}

	for (size_t i = 0; i < len; i += BASE64_BLOCK_SIZE)
	{
		SecureVector<uint8_t> tmp = CharBlockToVal(&buf[i]);
		res.insert(res.end(), tmp.begin(), tmp.end());
	}

	return res;
}

