#include "src/common/print_hex.h"

#include <assert.h>
#include <iomanip>

std::ostream& operator<<(std::ostream& os, const SecureVector<uint8_t> &buf)
{
	// Save format
	std::ios  state(nullptr);
	state.copyfmt(os);

	os << std::setfill('0') << std::setw(2) << std::hex;
	for (size_t i = 0; i < buf.size(); i++)
	{
		os << static_cast<const int>(buf[i]);
	}

	os.copyfmt(state);

	return os;
}

Buffer::Buffer(const uint8_t *buf_in, const size_t size_in)
	: data(buf_in)
	, size(size_in)
{
	assert(data != nullptr);
}

std::ostream& operator<<(std::ostream& os, const Buffer& buf)
{
	// Save format
	std::ios  state(nullptr);
	state.copyfmt(os);

	os << std::setfill('0') << std::setw(2) << std::hex;
	for (size_t i = 0; i < buf.size; i++)
	{
		os << static_cast<const int>(buf.data[i]);
	}

	os.copyfmt(state);

	return os;
}

