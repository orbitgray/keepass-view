#ifndef VERBOSE_STREAM_H
#define VERBOSE_STREAM_H

#include <iostream>
#include <iomanip>

// Singleton class for verbose output stream
class VerboseStream
{
public:
	void TurnOn();
	void TurnOff();

	static VerboseStream* Instance();

	template<typename T>
	VerboseStream& operator<<(T x)
	{
		if (turned_on)
			std::cout << x;
		return *this;
	}

	void SaveFormat();
	void LoadFormat();

private:
	VerboseStream() : state(nullptr) {}

	bool turned_on = false;
	static VerboseStream* singleton_instance;

	// For format save/load
	std::ios state;
};

#endif // VERBOSE_STREAM_H
