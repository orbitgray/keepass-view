#ifndef SECURE_VECTOR_H
#define SECURE_VECTOR_H

#include <iostream>
#include <vector>

#include "./src/common/secure_allocator.h"

template<typename T>
using SecureVector = std::vector<T, free_zero_allocator<T>>;

inline
std::istream &operator>>(std::istream &is, SecureVector<uint8_t> &vec)
{
	std::string str;
	std::getline(is, str);

	for (char c : str)
	{
		vec.push_back(static_cast<uint8_t>(c));
	}

	// make sure to clean tmp string
	for (size_t i = 0; i < str.size(); i++)
	{
		str[i] = '\0';
	}

	return is;
}

/*
template<typename T, class Allocator = free_zero_allocator<T>>
class SecureVector : public std::vector<T, class Allocator>
{
public:
	using vector<T, class Allocator>::vector; // use the constructors from vector

	// This is wrapper class to handle multiple writing.
	template<T>
	class Wrapper
	{
		Wrapper(SecureVector& sv, int pos_in) : object(sv), pos(pos_in) {}
		SecureVector& object;
		std::size_t pos;

	public:
		// If we read something
		operator T() const
		{
			return object.x[pos];
		}

		// If we write something
		Wrapper& operator=(T&& val)
		{
			// TODO: add counter for number of writes
			// TODO: encrypt content rather than storing plain text.
			object[pos] = val;
			return *this ;
		}

		friend class SecureVector;
	};

	// If we write more then once then we need to change cipher key
	// and update the vector just in case.
	T& operator[](int i) { return vector<T>::at(i); } // range-checked

	// We can read as many as we want.

	// Because we might change object and we dor't want to store plain versio.
	Wrapper operator[](std::size_t i)
	{
		return Wrapper(*this, i);
	}

	const T operator[](int i) const
	{
		// TODO: do decrypt
		return vector<T>::at(i);
	}

private:
	// Use this function only from wrapper
	// But don't expose them to public
	using T& vector<T, class Allocator>::operator[](int i);
};
*/

#endif // SECURE_VECTOR_H
