#ifndef COMPRESSION_H
#define COMPRESSION_H

#include "src/common/secure_vector.h"

#include <cstdint>
#include <cstddef>

class Compression
{
	const static int INIT2_WINDOW_BITS_DEFAULT   = 15;
	const static int INIT2_WINDOW_BITS_NO_HEADER = 16;
public:
	static bool Unpack(SecureVector<uint8_t> &buf_in, SecureVector<uint8_t> &buf_out);
	static bool Unpack(uint8_t* buf_in, const size_t buf_in_size, SecureVector<uint8_t> &buf_out);
};

#endif // COMPRESSION_H
