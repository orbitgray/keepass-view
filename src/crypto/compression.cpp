#include "src/crypto/compression.h"

#include <zlib.h>
#include <assert.h>
#include <iostream>

static void zerr(int ret)
{
	std::cerr << "zpipe: ";
	switch (ret) {
		case Z_ERRNO:
			std::cerr << "error reading stdin\n";
			break;
		case Z_STREAM_ERROR:
			std::cerr << "invalid compression level\n";
			break;
		case Z_DATA_ERROR:
			std::cerr << "invalid or incomplete deflate data\n";
			break;
		case Z_MEM_ERROR:
			std::cerr << "out of memory\n";
			break;
		case Z_VERSION_ERROR:
			std::cerr << "zlib version mismatch!\n";
	}
}


bool Compression::Unpack(SecureVector<uint8_t> &buf_in, SecureVector<uint8_t> &buf_out)
{
	return Unpack(buf_in.data(), buf_in.size(), buf_out);
}

bool Compression::Unpack(uint8_t* buf_in, const size_t buf_in_size, SecureVector<uint8_t> &buf_out)
{
	const size_t CHUNK = 16384;
	int res;
	z_stream strm;
	strm.zalloc = Z_NULL;
	strm.zfree	= Z_NULL;
	strm.opaque = Z_NULL;
	strm.next_in = buf_in;
	strm.avail_in = buf_in_size;
	strm.data_type = 0;

	// Allocating x2 from input size.
	// Very raw guess. We would need to adjust later.
	buf_out.resize(2 * buf_in_size);
	strm.next_out = buf_out.data();
	strm.avail_out = buf_out.size();

	// try zlib or gzip header
	res = inflateInit(&strm);
	if (res != Z_OK)
		return false;
	res = inflate(&strm, Z_BLOCK);

	// if fails try no headers gzip
	if (res != Z_OK)
	{
		// close previous stream
		res = inflateEnd(&strm);
		if (res != Z_OK)
		{
			std::cerr << "Closing compression stream has failed with err code = " << res << std::endl;
			zerr(res);
			return false;
		}

		// reset input buffer
		strm.next_in = buf_in;
		strm.avail_in = buf_in_size;

		// init without headers
		res = inflateInit2(&strm, INIT2_WINDOW_BITS_DEFAULT | INIT2_WINDOW_BITS_NO_HEADER);
		if (res != Z_OK)
			return false;
	}

	size_t output_size = buf_out.size();
	do
	{
		res = inflate(&strm, Z_SYNC_FLUSH | Z_FINISH);

		if (res == Z_OK && strm.avail_out == 0)
		{
			// increase output buffer if we run out of output space
			buf_out.resize(output_size + CHUNK);
			strm.next_out = buf_out.data() + output_size;
			strm.avail_out = CHUNK;
			output_size = buf_out.size();
		}
	} while (res == Z_OK);

	if (res != Z_STREAM_END)
	{
		std::cerr << "Unpacking has failed with err code = " << res << std::endl;
		zerr(res);
		return false;
	}

	res = inflateEnd(&strm);
	if (res != Z_OK)
	{
		std::cerr << "Closing compression stream has failed with err code = " << res << std::endl;
		zerr(res);
		return false;
	}

	// adjult output buffer size
	output_size -= strm.avail_out;
	buf_out.resize(output_size);

	return true;
}
