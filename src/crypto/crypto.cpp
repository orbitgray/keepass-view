#include "src/crypto/crypto.h"
#include "src/common/print_hex.h"
#include "src/common/verbose_stream.h"

#include <iostream>
#include <assert.h>
#include <gcrypt.h>
#include <string.h>


/********************************************
 *
 * Generic class to init GCrypt
 *
 ********************************************/


bool Crypto::was_init = false;
std::mutex Crypto::init_mutex;

Crypto::Crypto()
{
	init_mutex.lock();

	if (was_init)
	{
		init_mutex.unlock();
		return;
	}

	if (!InitGCrypt())
	{
		std::cerr << "Error initializing GCrypt." << std::endl;
		exit(-1);
	}
	was_init = true;

	init_mutex.unlock();
}

Crypto::~Crypto()
{
}

static bool IsPadded(const uint8_t *buf, const size_t buf_size, size_t &padding_size)
{
	if (buf == nullptr)
		return false;

	size_t padding_number = buf[buf_size - 1];
	
	if (padding_number > buf_size)
		return false;

	for (size_t i = 0; i < padding_number; i++)
	{
		if (buf[buf_size - i - 1] != padding_number)
			return false;
	}

	return true;
}

// Can be single tone, than we don't need static crypto class
bool Crypto::InitGCrypt()
{
	// Version check should be the very first call because it
	// makes sure that important subsystems are initialized.
	if (!gcry_check_version (GCRYPT_VERSION))
	{
		std::cerr << "libgcrypt version mismatch." << std::endl;
		exit (2);
	}
	*VerboseStream::Instance() << "GCrypt version checked: " << GCRYPT_VERSION << "\n";

	// Disable secure memory.
	gcry_control (GCRYCTL_DISABLE_SECMEM, 0);

	// Tell Libgcrypt that initialization has completed.
	gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

	return true;
}


/********************************************
 *
 * Sha256
 *
 ********************************************/


bool Sha256::Hash(const uint8_t *buf_in, const size_t size_in, uint8_t *buf_out)
{
	if (buf_in == nullptr)
		return false;

	if (buf_out == nullptr)
		return false;
	
	gcry_md_hash_buffer(GCRY_MD_SHA256, buf_out, buf_in, size_in);
	return true;
}

bool Sha256::Hash(const SecureVector<uint8_t> buf_in, uint8_t *buf_out)
{
	return Hash(buf_in.data(), buf_in.size(), buf_out);
}

bool Sha256::Hash(const SecureVector<uint8_t> buf_in, SecureVector<uint8_t> &buf_out)
{
	size_t output_pos = buf_out.size();
	buf_out.resize(buf_out.size() + HashLength());
	return Hash(buf_in.data(), buf_in.size(), &buf_out[output_pos]);
}

size_t Sha256::HashLength() const
{
	return gcry_md_get_algo_dlen(GCRY_MD_SHA256);
}


/********************************************
 *
 * AES256
 *
 ********************************************/


void Aes256::AdjustBuffer(SecureVector<uint8_t> &buf)
{
	size_t res = buf.size() % gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256);

	if (res != 0)
	{
		size_t padding = gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256) - res;

		// resize vector
		buf.resize(buf.size() + padding);

		// Add padding
		for (size_t i = buf.size() - padding - 1; i < buf.size(); i++)
		{
			buf[i] = padding;
		}
	}
}

bool Aes256::Encrypt(const uint8_t *buf_in, const size_t buf_in_size, uint8_t *buf_out, const size_t buf_out_size, const Context& context)
{
	gcry_cipher_hd_t hd;
	gcry_error_t res;

	assert((buf_in_size <= buf_out_size) || (buf_out == nullptr && buf_out_size == 0));
	if ((buf_in_size > buf_out_size) && !(buf_out == nullptr && buf_out_size == 0))
	{
		std::cerr << "Output buffer size(" << buf_out_size << ") is smaller than input buffer size(" << buf_in_size << ")." << std::endl;
		return false;
	}

	assert(buf_in_size % gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256) == 0);
	if (buf_in_size % gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256) != 0)
	{
		std::cerr << "AES input buffer(" << buf_in_size << ") is not multiple of block size(" << gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256) << ")." << std::endl;
		// should not be hard to handle this case, but we don't expect it to happen becaul input buffer would be output of SHA256.
		return false;
	}

	// Init cipher
	res = gcry_cipher_open(&hd, GCRY_CIPHER_AES256, GetMode(), 0 /* flags */);
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error initializing cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Set IV
	res = gcry_cipher_setiv(hd, &context.iv[0], sizeof(context.iv));
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error setting IV for cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Set Key
	res = gcry_cipher_setkey(hd, &context.key[0], sizeof(context.key));
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error setting Key for cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Encryption
	for (size_t i = 0; i < context.rounds; i++)
	{
		res = gcry_cipher_encrypt (hd, buf_out, buf_out_size, buf_in, buf_in_size);
		if (res != GPG_ERR_NO_ERROR)
		{
			std::cerr << "Error encrypting with aes256ecb cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
			return false;
		}
	}

	// Close cipher
	gcry_cipher_close(hd);

	return true;
}

bool Aes256::Encrypt(uint8_t *buf_in_out, const size_t buf_in_size, const Context& context)
{
	Encrypt(nullptr, 0, buf_in_out, buf_in_size, context);
}

bool Aes256::Encrypt(SecureVector<uint8_t> &buf_in_out, Context& context)
{
	AdjustBuffer(buf_in_out);
	Encrypt(nullptr, 0, buf_in_out.data(), buf_in_out.size(), context);
}

bool Aes256::Encrypt(const SecureVector<uint8_t> &buf_in, SecureVector<uint8_t> &buf_out, Context& context)
{
	// AdjustBuffer(buf_in);
	buf_out = buf_in;
	Encrypt(nullptr, 0, buf_out.data(), buf_out.size(), context);
}


bool Aes256::Decrypt(const uint8_t *buf_in, const size_t buf_in_size, uint8_t *buf_out, const size_t buf_out_size, const Context& context)
{
	gcry_cipher_hd_t hd;
	gcry_error_t res;

	assert((buf_in_size <= buf_out_size) || (buf_out == nullptr && buf_out_size == 0));
	if ((buf_in_size > buf_out_size) && !(buf_out == nullptr && buf_out_size == 0))
	{
		std::cerr << "Output buffer size(" << buf_out_size << ") is smaller than input buffer size(" << buf_in_size << ")." << std::endl;
		return false;
	}

	// Init cipher
	res = gcry_cipher_open(&hd, GCRY_CIPHER_AES256, GetMode(), 0 /* flags */);
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error initializing cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Set IV
	res = gcry_cipher_setiv(hd, &context.iv[0], sizeof(context.iv));
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error setting IV for cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Set Key
	res = gcry_cipher_setkey(hd, &context.key[0], sizeof(context.key));
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error setting Key for cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	// Decryption
	for (size_t i = 0; i < context.rounds; i++)
	{
		res = gcry_cipher_decrypt(hd, buf_out, buf_out_size, buf_in, buf_in_size);
		if (res != GPG_ERR_NO_ERROR)
		{
			std::cerr << "Error decrypting with aes256ecb cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
			return false;
		}
	}

	size_t padding = 0;

	if (!IsPadded(buf_out, buf_in_size, padding))
	{
		std::cerr << "Found bad padding after decryption" << std::endl;
		return false;
	}
	// TODO: return this back

	// Close cipher
	gcry_cipher_close(hd);

	return true;
}

size_t Aes256::KeyLength()
{
	return gcry_cipher_get_algo_keylen(GCRY_CIPHER_AES256);
}

size_t Aes256::BlockLength()
{
	return gcry_cipher_get_algo_blklen(GCRY_CIPHER_AES256);
}

int Aes256Ecb::GetMode() const
{
	return GCRY_CIPHER_MODE_ECB;
}

int Aes256Cbc::GetMode() const
{
	return GCRY_CIPHER_MODE_CBC;
}


/********************************************
 *
 * Salsa20
 *
 ********************************************/


bool Salsa20::SetContext(const Context& context)
{
	gcry_error_t res;

	if (context.key.empty())
	{
		std::cerr << "Bad context provided for Salsa20 cipher. (key is empty)" << std::endl;
		return false;
	}

	res = gcry_cipher_open(&hd, GCRY_CIPHER_SALSA20, GCRY_CIPHER_MODE_STREAM, 0 /* flags */);
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error initializing Salsa20 cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	res = gcry_cipher_setkey(hd, context.key.data(), context.key.size());
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error setting Salsa20 key: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		gcry_cipher_close(hd);
		return false;
	}

	if (!context.iv.empty())
	{
		res = gcry_cipher_setiv(hd, context.iv.data(), context.iv.size());
		if (res != GPG_ERR_NO_ERROR)
		{
			std::cerr << "Error setting Salsa20 iv: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
			gcry_cipher_close(hd);
			return false;
		}
	}

	return true;
}

bool Salsa20::Encrypt(const SecureVector<uint8_t> &block_in, SecureVector<uint8_t> &block_out)
{
	gcry_error_t res;

	block_out.resize(block_in.size());

	std::cerr << "Salsa input = " << block_in << std::endl;

	res = gcry_cipher_encrypt(hd, block_out.data(), block_out.size(), block_in.data(), block_in.size());
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error encrypting with Salsa20 cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	return true;
}

bool Salsa20::Decrypt(const SecureVector<uint8_t> &block_in, SecureVector<uint8_t> &block_out)
{
	gcry_error_t res;

	block_out.resize(block_in.size());

	res = gcry_cipher_decrypt(hd, block_out.data(), block_out.size(), block_in.data(), block_in.size());
	if (res != GPG_ERR_NO_ERROR)
	{
		std::cerr << "Error decrypting with Salsa20 cipher: " << gcry_strsource(res) << "/" << gcry_strerror(res) << std::endl;
		return false;
	}

	return true;
}
