#ifndef CRYPTO_H
#define CRYPTO_H

#include "src/common/secure_vector.h"

#include <gcrypt.h>

#include <cstddef>
#include <cstdint>
#include <mutex>


// TODO: split different codecs in different files.

/********************************************
 *
 * Generic class to init GCrypt
 *
 ********************************************/


class Crypto
{
	static bool was_init;
	static std::mutex init_mutex;

public:
	Crypto();
	~Crypto();

private:
	static bool InitGCrypt();
};


/********************************************
 *
 * Sha256
 *
 ********************************************/


class Sha256: public Crypto
{
public:
	bool Hash(const uint8_t *buf_in, const size_t size_in, uint8_t *buf_out);
	bool Hash(const SecureVector<uint8_t> buf_in, uint8_t *buf_out);
	bool Hash(const SecureVector<uint8_t> buf_in, SecureVector<uint8_t> &buf_out);
	size_t HashLength() const;
};


/********************************************
 *
 * AES256
 *
 ********************************************/


class Aes256 : public Crypto
{
public:
	// aes-256 iv, key and rounds
	struct Context
	{
		uint8_t iv[16] = {0};
		uint8_t key[32] = {0};
		uint64_t rounds = 0;
	};

	bool Encrypt(SecureVector<uint8_t> &buf_in_out, Context& context);
	bool Encrypt(const SecureVector<uint8_t> &buf_in, SecureVector<uint8_t> &buf_out, Context& context);

	bool Encrypt(uint8_t *buf_in_out, const size_t buf_in_size, const Context& context);
	bool Encrypt(const uint8_t *buf_in, const size_t buf_in_size, uint8_t *buf_out, const size_t buf_out_size, const Context& context);

	bool Decrypt(SecureVector<uint8_t> &buf_in_out, Context& context);
	bool Decrypt(const SecureVector<uint8_t> &buf_in, SecureVector<uint8_t> &buf_out, Context& context);

	bool Decrypt(uint8_t *buf_in_out, const size_t buf_in_size, const Context& context);
	bool Decrypt(const uint8_t *buf_in, const size_t buf_in_size, uint8_t *buf_out, const size_t buf_out_size, const Context& context);

	static size_t KeyLength();
	static size_t BlockLength();

	virtual int GetMode() const = 0;

private:
	// Adjusts vector size to be multiple of input/output block size
	void AdjustBuffer(SecureVector<uint8_t> &buf);
};

class Aes256Cbc : public Aes256
{
public:
	virtual int GetMode() const override;
};

class Aes256Ecb : public Aes256
{
public:
	virtual int GetMode() const override;
};

/********************************************
 *
 * Salsa20
 *
 ********************************************/


class Salsa20: public Crypto
{
	// This is stream cipher, and we need to store state for it.
	// Unlike with sha or aes.
	gcry_cipher_hd_t hd;
public:

	struct Context
	{
		SecureVector<uint8_t> key;
		SecureVector<uint8_t> iv;
	};

	bool SetContext(const Context& context);

	bool Encrypt(const SecureVector<uint8_t> &block_in, SecureVector<uint8_t> &block_out);
	bool Decrypt(const SecureVector<uint8_t> &block_in, SecureVector<uint8_t> &block_out);
};

#endif // CRYPTO_H
