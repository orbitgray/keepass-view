cmake_minimum_required(VERSION 3.3)

project(keepass-view)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

find_package(GCrypt REQUIRED)
find_package(ZLIB REQUIRED)
find_package(LibXml2 REQUIRED)

include_directories(SYSTEM
	${GCRYPT_INCLUDE_DIR}
	${ZLIB_INCLUDE_DIR}
	${LIBXML2_INCLUDE_DIR}
	)

set (keepass_view_VERSION_MAJOR 0)
set (keepass_view_VERSION_MINOR 4)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file(
	"${PROJECT_SOURCE_DIR}/src/keepass_view_config.h.in"
	"${PROJECT_BINARY_DIR}/keepass_view_config.h"
	)

# add the binary tree to the search path for include files
# so that we will find *_config.h
include_directories(
	"${PROJECT_BINARY_DIR}"
	"${PROJECT_SOURCE_DIR}"
)

# Sources of kdbx file format parsing.
set(keepass_view_SOURCES_FORMAT
	./src/common/print_hex.cpp
	./src/common/base64.cpp
	./src/common/verbose_stream.cpp
	./src/common/debug_stream.cpp
	./src/crypto/crypto.cpp
	./src/crypto/compression.cpp
	./src/format/read.cpp
	./src/format/kdbx_file.cpp
	./src/format/kdbx_file_header.cpp
	./src/format/kdbx_file_header_entry.cpp
	./src/format/kdbx_file_payload.cpp
	./src/format/kdbx_file_payload_xml.cpp
	./src/format/composite_key.cpp
	./src/format/master_key.cpp
	./src/format/decrypt_payload.cpp
	./src/format/payload_block.cpp
	./src/format/protected_stream.cpp
)

# CLI sources
set(keepass_view_SOURCES_CLI
	./src/cli/main.cpp
)

add_executable(keepass-view 
	${keepass_view_SOURCES_FORMAT}
	${keepass_view_SOURCES_CLI}
	)

target_link_libraries(keepass-view
	${GCRYPT_LIBRARIES}
	${ZLIB_LIBRARIES}
	${LIBXML2_LIBRARIES}
	)

target_compile_features(keepass-view PRIVATE
							cxx_nullptr
							# cxx_relaxed_constexpr
						)

# Build tests if we have GTest
find_package(GTest)
if (GTEST_FOUND)
	enable_testing()
	add_subdirectory(test)
endif()
