#include "gtest/gtest.h"

#include "src/common/base64.h"


class Base64Test : public ::testing::Test
{
};

class TestableBase64 : public Base64
{
public:
	using Base64::BASE64_BLOCK_SIZE;
	using Base64::BASE64_PADDING;
	using Base64::BASE64_SPACIAL_1;
	using Base64::BASE64_SPACIAL_2;

	using Base64::CharToVal;
	using Base64::BlockPadLength;
	using Base64::CharBlockToVal;
};


TEST(Base64Test, SpecialSymbols)
{
	ASSERT_EQ('+', TestableBase64::BASE64_SPACIAL_1);
	ASSERT_EQ('/', TestableBase64::BASE64_SPACIAL_2);
}

TEST(Base64Test, CharToValGood)
{
	const char* test = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	// Check A-Za-z0-0 symbols
	size_t i;
	for (i = 0; i < strlen(test); i++)
	{
		// std::cout << test[i] << " = " << (int)TestableBase64::CharToVal(test[i]) << std::endl;
		ASSERT_EQ(i, (int)TestableBase64::CharToVal(test[i]));
	}

	// Check special symbols
	ASSERT_EQ(i++, TestableBase64::CharToVal(TestableBase64::BASE64_SPACIAL_1));
	ASSERT_EQ(i++, TestableBase64::CharToVal(TestableBase64::BASE64_SPACIAL_2));
}

TEST(Base64Test, CharToValBad)
{
#ifdef NDEBUG
	// no asserts, return 0
	const char* test = ".,!@#%$^&*()_-|\\|";

	// Check A-Za-z0-0 symbols
	size_t i;
	for (i = 0; i < strlen(test); i++)
	{
		// std::cout << test[i] << " = " << (int)TestableBase64::CharToVal(test[i]) << std::endl;
		ASSERT_EQ((int)TestableBase64::CharToVal(test[i]), 0);
	}
#else
	// expect asserts
	const char* test = ".,!@#%$^&*()_-|\\|";

	// Check A-Za-z0-0 symbols
	size_t i;
	for (i = 0; i < strlen(test); i++)
	{
		// std::cout << test[i] << " = " << (int)TestableBase64::CharToVal(test[i]) << std::endl;
		EXPECT_DEATH((int)TestableBase64::CharToVal(test[i]), "Found not Base64 character.");
	}
#endif
}

TEST(Base64Test, BlockPadLengthDummy)
{
	ASSERT_EQ('=', TestableBase64::BASE64_PADDING);
	ASSERT_EQ(4, TestableBase64::BASE64_BLOCK_SIZE);
}

TEST(Base64Test, BlockPadLengthBlockTooShort)
{
#ifdef NDEBUG
	EXPECT_EQ(TestableBase64::BlockPadLength("a"),   0);
	EXPECT_EQ(TestableBase64::BlockPadLength("aa"),  0);
	EXPECT_EQ(TestableBase64::BlockPadLength("aaa"), 0);

	EXPECT_EQ(TestableBase64::BlockPadLength("="),   0);
	EXPECT_EQ(TestableBase64::BlockPadLength("=="),  0);
	EXPECT_EQ(TestableBase64::BlockPadLength("==="), 0);

	EXPECT_EQ(TestableBase64::BlockPadLength("a="),  0);
	EXPECT_EQ(TestableBase64::BlockPadLength("aa="), 0);
	EXPECT_EQ(TestableBase64::BlockPadLength("a=="), 0);
#else
	EXPECT_DEATH(TestableBase64::BlockPadLength("a"),   "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("aa"),  "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("aaa"), "strlen\\(s\\) >= 4");

	EXPECT_DEATH(TestableBase64::BlockPadLength("="),   "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("=="),  "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("==="), "strlen\\(s\\) >= 4");

	EXPECT_DEATH(TestableBase64::BlockPadLength("a="),  "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("aa="), "strlen\\(s\\) >= 4");
	EXPECT_DEATH(TestableBase64::BlockPadLength("a=="), "strlen\\(s\\) >= 4");
#endif
}

TEST(Base64Test, BlockPadLength1)
{
	// Function only cares about first block
	// So second block can be anything
	EXPECT_EQ(1, TestableBase64::BlockPadLength("abc="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcd="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcde="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdef="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdefg="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdefgh="));
}

TEST(Base64Test, BlockPadLength2)
{
	// Function only cares about first block
	// So second block can be anything
	EXPECT_EQ(2, TestableBase64::BlockPadLength("ab=="));
	EXPECT_EQ(1, TestableBase64::BlockPadLength("abc=="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcd=="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcde=="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdef=="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdefg=="));
	EXPECT_EQ(0, TestableBase64::BlockPadLength("abcdefgh=="));
}

TEST(Base64Test, BlockPadLengthBad3)
{
	// Function only cares about first block
	// So second block can be anything
	// But 3 should never be a correct result
#ifdef NDEBUG
	EXPECT_EQ(TestableBase64::BlockPadLength("a==="), 0);
	EXPECT_EQ(TestableBase64::BlockPadLength("===="), 0);
#else
	EXPECT_DEATH(TestableBase64::BlockPadLength("a==="), "res < 3");
	EXPECT_DEATH(TestableBase64::BlockPadLength("===="), "res < 3");
#endif
}

// Some test vectors from RFC4648
// https://tools.ietf.org/html/rfc4648
// CharBlockToValGood is internal function and handles only first block of the string.
TEST(Base64Test, CharBlockToValGood)
{
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zg=="), SecureVector<uint8_t>({'f'}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zm8="), SecureVector<uint8_t>({'f', 'o'}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zm9v"), SecureVector<uint8_t>({'f', 'o', 'o'}));
}

TEST(Base64Test, CharBlockToValBad)
{
#ifdef NDEBUG
	EXPECT_EQ(TestableBase64::CharBlockToVal("a"),   SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("aa"),  SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("aaa"), SecureVector<uint8_t>({0, 0, 0}));

	EXPECT_EQ(TestableBase64::CharBlockToVal("="),   SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("=="),  SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("==="), SecureVector<uint8_t>({0, 0, 0}));

	EXPECT_EQ(TestableBase64::CharBlockToVal("a="),  SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("aa="), SecureVector<uint8_t>({0, 0, 0}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("a=="), SecureVector<uint8_t>({0, 0, 0}));
#else
	EXPECT_DEATH(TestableBase64::CharBlockToVal("a"),   "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("aa"),  "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("aaa"), "Base64 block is shorter than expected");

	EXPECT_DEATH(TestableBase64::CharBlockToVal("="),   "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("=="),  "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("==="), "Base64 block is shorter than expected");

	EXPECT_DEATH(TestableBase64::CharBlockToVal("a="),  "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("aa="), "Base64 block is shorter than expected");
	EXPECT_DEATH(TestableBase64::CharBlockToVal("a=="), "Base64 block is shorter than expected");
#endif
}

// Some test vectors from RFC4648
// https://tools.ietf.org/html/rfc4648
// CharBlockToValGood is internal function and handles only first block of the string.
TEST(Base64Test, CharBlockToValSecondBlockDoesntMatter)
{
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zm9vYg=="), SecureVector<uint8_t>({'f', 'o', 'o'}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zm9vYmE="), SecureVector<uint8_t>({'f', 'o', 'o'}));
	EXPECT_EQ(TestableBase64::CharBlockToVal("Zm9vYmFy"), SecureVector<uint8_t>({'f', 'o', 'o'}));
}

// Some test vectors from RFC4648
// https://tools.ietf.org/html/rfc4648
// DecodeGood is internal function and handles only first block of the string.
TEST(Base64Test, DecodeGood)
{
	EXPECT_EQ(TestableBase64::Decode("Zg=="),     SecureVector<uint8_t>({'f'}));
	EXPECT_EQ(TestableBase64::Decode("Zm8="),     SecureVector<uint8_t>({'f', 'o'}));
	EXPECT_EQ(TestableBase64::Decode("Zm9v"),     SecureVector<uint8_t>({'f', 'o', 'o'}));
	EXPECT_EQ(TestableBase64::Decode("Zm9vYg=="), SecureVector<uint8_t>({'f', 'o', 'o', 'b'}));
	EXPECT_EQ(TestableBase64::Decode("Zm9vYmE="), SecureVector<uint8_t>({'f', 'o', 'o', 'b', 'a'}));
	EXPECT_EQ(TestableBase64::Decode("Zm9vYmFy"), SecureVector<uint8_t>({'f', 'o', 'o', 'b', 'a', 'r'}));
}

TEST(Base64Test, DecodeBad)
{
#ifdef NDEBUG
	EXPECT_EXIT(TestableBase64::Decode("a"),   ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("aa"),  ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("aaa"), ::testing::ExitedWithCode(255), "Malformed base64 string:");

	EXPECT_EXIT(TestableBase64::Decode("="),   ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("=="),  ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("==="), ::testing::ExitedWithCode(255), "Malformed base64 string:");

	EXPECT_EXIT(TestableBase64::Decode("a="),  ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("aa="), ::testing::ExitedWithCode(255), "Malformed base64 string:");
	EXPECT_EXIT(TestableBase64::Decode("a=="), ::testing::ExitedWithCode(255), "Malformed base64 string:");
#else
	EXPECT_DEATH(TestableBase64::Decode("a"),   "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("aa"),  "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("aaa"), "\\(len % BASE64_BLOCK_SIZE\\) == 0");

	EXPECT_DEATH(TestableBase64::Decode("="),   "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("=="),  "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("==="), "\\(len % BASE64_BLOCK_SIZE\\) == 0");

	EXPECT_DEATH(TestableBase64::Decode("a="),  "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("aa="), "\\(len % BASE64_BLOCK_SIZE\\) == 0");
	EXPECT_DEATH(TestableBase64::Decode("a=="), "\\(len % BASE64_BLOCK_SIZE\\) == 0");
#endif
}

