#include "gtest/gtest.h"

#include <memory>
#include <climits>
#include <./src/common/secure_allocator.h>

class VanillaAllocator : public ::testing::TestWithParam<size_t>
{
};

// This not rawbust test
// Just testing if we have a lot of data leaked with standard allocator.
// For small numbers number of matches is very low
TEST_P (VanillaAllocator, MemoryNotChangedChar)
{
	return;
	const size_t SIZE = GetParam();
	std::allocator<unsigned char> a;
	unsigned char* s1 = nullptr;
	s1 = a.allocate(SIZE);
	ASSERT_NE(s1, nullptr);

	for (size_t i = 0; i < SIZE; i++)
		s1[i] = i;

	for (size_t i = 0; i < SIZE; i++)
		EXPECT_EQ(s1[i], (unsigned char)i);

	a.deallocate(s1, SIZE);

	// if we are lucky we'll get same memory with same data
	unsigned char* s2 = nullptr;
	s2 = a.allocate(SIZE);
	ASSERT_NE(s2, nullptr);
	EXPECT_EQ(s1, s2);
	size_t count_match = 0;
	for (size_t i = 0; i < SIZE; i++)
	{
		if (s2[i] == (unsigned char)i)
		{
			count_match++;
		}
	}

	// With small allocations we get smaller match values for some reason.
	// It probably has to do with different allocation mechanisms used.
	if (SIZE > 200)
		EXPECT_GE(count_match, SIZE*0.9);
	else if (SIZE > 30)
		EXPECT_GE(count_match, SIZE*0.5);
	else if (SIZE > 10)
		EXPECT_GE(count_match, SIZE*0.3);
	else
		EXPECT_GE(count_match, SIZE*0.1);

	// std::cout << count_match << " / " << SIZE << std::endl;
}

// Same test as MemoryNotChangedChar
// But use unsafe memory check using dangling pointer.
TEST_P (VanillaAllocator, MemoryNotChangedCharUnsafe)
{
	const size_t SIZE = GetParam();
	std::allocator<unsigned char> a;
	unsigned char* s1 = nullptr;
	s1 = a.allocate(SIZE);
	ASSERT_NE(s1, nullptr);

	for (size_t i = 0; i < SIZE; i++)
		s1[i] = i;

	for (size_t i = 0; i < SIZE; i++)
		EXPECT_EQ(s1[i], (unsigned char)i);

	a.deallocate(s1, SIZE);

	// Not safe check based on dangling pointer.
	unsigned char* s2 = s1;
	size_t count_match = 0;
	for (size_t i = 0; i < SIZE; i++)
	{
		if (s2[i] == (unsigned char)i)
		{
			count_match++;
		}
	}

	// With small allocations we get smaller match values for some reason.
	// It probably has to do with different allocation mechanisms used.
	if (SIZE > 200)
		EXPECT_GE(count_match, SIZE*0.9);
	else if (SIZE > 30)
		EXPECT_GE(count_match, SIZE*0.4);
	else
		EXPECT_GE(count_match, 1);

	// std::cout << count_match << " / " << SIZE << std::endl;
}

// Same test as MemoryNotChangedCharUsafe
// But zero memory before freeing.
// Emulating free_zero_allocator behavior.
// Should get same results.
TEST_P (VanillaAllocator, MemoryNotChangedZeroCharUnsafe)
{
	const size_t SIZE = GetParam();
	std::allocator<unsigned char> a;
	unsigned char* s1 = nullptr;
	s1 = a.allocate(SIZE);
	ASSERT_NE(s1, nullptr);

	memset(s1, 0, SIZE);

	for (size_t i = 0; i < SIZE; i++)
		EXPECT_EQ(s1[i], 0);

	a.deallocate(s1, SIZE);

	// Not safe check based on dangling pointer.
	unsigned char* s2 = s1;
	size_t count_match = 0;
	for (size_t i = 0; i < SIZE; i++)
	{
		if (s2[i] == (unsigned char)i)
		{
			count_match++;
		}
	}

	EXPECT_LE(count_match, SIZE/UCHAR_MAX + 1); // Just 0 should match, which happens on unsigned char wrap.
	// std::cout << count_match << " / " << SIZE << std::endl;
}

INSTANTIATE_TEST_CASE_P(TestWithParameters, VanillaAllocator, ::testing::Range((size_t)1, (size_t)1000, 10));


class FreeZeroAllocator : public ::testing::TestWithParam<size_t>
{
};

TEST_P (FreeZeroAllocator, FreeZeroGetZero)
{
	const size_t SIZE = GetParam();
	free_zero_allocator<unsigned char> a;
	unsigned char* s1 = nullptr;
	s1 = a.allocate(SIZE);
	ASSERT_NE(s1, nullptr);

	memset(s1, 0, SIZE);

	for (size_t i = 0; i < SIZE; i++)
		EXPECT_EQ(s1[i], 0);

	a.deallocate(s1, SIZE);

	// Not safe check based on dangling pointer.
	unsigned char* s2 = s1;
	size_t count_match = 0;
	for (size_t i = 0; i < SIZE; i++)
	{
		if (s2[i] == (unsigned char)i)
		{
			count_match++;
		}
	}

	EXPECT_LE(count_match, SIZE/UCHAR_MAX + 1); // Just 0 should match, which happens on unsigned char wrap.
	// std::cout << count_match << " / " << SIZE << std::endl;
}

TEST_P (FreeZeroAllocator, FreeRandGetZero)
{
	const size_t SIZE = GetParam();
	free_zero_allocator<unsigned char> a;
	unsigned char* s1 = nullptr;
	s1 = a.allocate(SIZE);
	ASSERT_NE(s1, nullptr);

	for (size_t i = 0; i < SIZE; i++)
		s1[i] = i;

	for (size_t i = 0; i < SIZE; i++)
		EXPECT_EQ(s1[i], (unsigned char)i);

	a.deallocate(s1, SIZE);

	// Not safe check based on dangling pointer.
	unsigned char* s2 = s1;
	size_t count_match = 0;
	for (size_t i = 0; i < SIZE; i++)
	{
		if (s2[i] == (unsigned char)i)
		{
			count_match++;
		}
	}

	EXPECT_LE(count_match, SIZE/UCHAR_MAX + 1); // Just 0 should match, which happens on unsigned char wrap.
	// std::cout << count_match << " / " << SIZE << std::endl;
}

INSTANTIATE_TEST_CASE_P(TestWithParameters, FreeZeroAllocator, ::testing::Range((size_t)1, (size_t)1000, 10));
